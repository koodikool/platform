import jsTokens from "js-tokens"
import * as htmlTokenizer from 'html-tokenizer'
import * as cssTokenizer from '@csstools/tokenizer'
const htmlTokens = htmlTokenizer.Tokenizer.tokenize
const cssTokens = cssTokenizer.tokenize

const cssTokenTypes = {
    1: "Symbol",
    2: "Comment",
    3: "Space",
    4: "Word",
    5: "Function",
    6: "Atword",
    7: "Hash",
    8: "String",
    9: "Number",
}

const htmlExplanations = {
    'opening-tag': ({ name }) => ([
        {
            data: '<',
            title: 'HTML tag opening symbol',
            type: 'Symbol'
        },
        {
            data: name,
            type: 'Tag'
        }
    ]),
    'attribute': ({ name, value }) => ([
        {
            type: 'Whitespace',
            data: ' ',
            title: 'Whitespace'
        }, {
            type: 'Attribute',
            data: name,
            title: `This is an attribute. Think of it like settings for the HTML tag. The syntax is key="value" and key's are pre-defined for every tag.`
        }, {
            type: 'Symbol',
            data: '=',
            title: 'Set a value to the attribute.'
        }, truncate_token({
            type: 'String',
            data: `"${value}"`,
            title: `This is the attribute value. It's always a string.`
        })
    ]),
    'opening-tag-end': ({ token }) => {
        if (token === '>') {
            return {
                type: 'Symbol',
                data: `>`,
                title: `This symbol ends the 'opening' of the tag. There will also be a 'closing' of this tag.`
            }
        }
        return {
            type: 'Symbol',
            data: `/>`,
            title: `This HTML tag is self-closing. It can't have any content.`
        }
    },
    'text': ({ text }) => ({
        type: 'Value',
        data: text,
        title: `This is the content of the HTML tag, because it's in between the 'opening' and 'closing' parts. Example: <p>content</p>`
    }),
    'closing-tag': ({ name }) => ({
        type: 'Symbol',
        data: `</${name}>`,
        title: `The '${name}' is now 'closing'.`
    }),
    'comment': ({ text }) => ({
        type: 'Comment',
        data: `</${text}>`,
        title: `Just a comment for reading. Has no functional effect on the code.`
    }),
}

const cssExplanations = {
    ":": "This colon separates a CSS command from it's value.",
    ";": "Semicolon ends the statement. You can't write the next line without it in CSS.",
    "{": "Once an element is selected - write CSS commands in between the curly brackets to change the elements appearence.",
    "}": "Ends the commands block.",
    "#": "When you add an 'id' to an element in HTML (i.e. <input id='email'/>) then you can select that element in CSS with the hastag (i.e. #email).",
    ".": "When you add one or more classes to an element in HTML (i.e. <div class='send-button'/>) then you can select that element in CSS with the dot (i.e. .send-button).",
    "*": "The asterisk selects every element on the page, individually. You can apply a styling command to all elements at the same time.",
    "important": "!important overrides other commands. For example if you set two colors to the same element, !important will make sure that color is selected that you want."
}

const jsExplanations = {
    ".": "The dot gives access to a key inside an object. So for human = {age: 20, height: 192} you can access values with human.age or human.height.",
    "(": "Brackets are used to run functions (i.e. querySelector('#email')) or define new functions (i.e. function getEmail() {}) or for some statements (i.e. if (true) {})",
    ")": "All brackets always need to end. Otherwise it's a syntax error and the computer wont know where to end a section.",
    "=": "Assign a value into a variable with the 'equals' symbol.",
    ",": "The comma is a separator of arguments.",
    "document": `This object contains all of the DOM on the page (DOM is basically your HTML). If you want to search the whole page for an element use document.querySelector("css-selector").`,
    "querySelector": `Use a CSS selector to find any element on the page with 'document.querySelector("css-selector")'.`,
    "var": `Declare a new variable. This syntax is old, use 'const' and 'let' instead.`,
    "const": `Define a variable that doesn't change (i.e. const daysInYear = 365). Note: if the variable is an array or an object, then the contents are still allowed to change.`,
    "let": `Define a variable that does change (i.e. let weekdayToday = "monday").`,
    "StringLiteral": `Strings are useful for holding data that can be represented in text form.`,
    "fetchPOST": `Makes a POST request. User to send data to a server. A simpler version of the JavaScript command fetch(). Only available on Tinkr.`,
    "fetchGET": `Makes a GET request. User to ask data from a server. A simpler version of the JavaScript command fetch(). Only available on Tinkr.`,
}

function tokenizeAndStandardise(code, lang) {
    switch (lang) {
        case "html": return tokenizeAndStandardiseHTML(code); break;
        case "css": return tokenizeAndStandardiseCSS(code); break;
        case "js": return tokenizeAndStandardiseJS(code); break;
    }
}

function tokenizeAndStandardiseHTML(code) {
    let result = []
    for (const token of htmlTokens(code)) {
        if (['start', 'done'].includes(token.type)) {
            continue
        }
        result.push(htmlExplanations[token.type](token))
    }
    result = result.flat()
    const terms = result.filter(({ type }) => ['Attribute', 'Tag'].includes(type)).map(r => r.data)
    const mdn = mdnSearch(terms, 'html')
    return result.map(r => ({ ...r, mdn_path: mdn[r.data]?.url, title: mdn[r.data]?.title || r.title }))
}

function tokenizeAndStandardiseCSS(code) {
    const result = []
    for (const cssToken of cssTokens(code)) {
        result.push(truncate_token({
            type: cssTokenTypes[cssToken.type],
            data: cssToken.lead + cssToken.data + cssToken.tail,
            title: cssExplanations[cssToken.lead] || cssExplanations[cssToken.data],
        }))
    }
    const terms = result.filter(({ type }) => ["Word", "Function"].includes(type)).map(r => r.data)
    const mdn = mdnSearch(terms, 'css')
    return result.map(r => ({ ...r, mdn_path: mdn[r.data]?.url, title: r.title || mdn[r.data]?.title || r.type }))

}

function tokenizeAndStandardiseJS(code) {
    const result = []
    for (const jsToken of jsTokens(code)) {
        result.push(truncate_token({
            type: jsToken.type,
            data: jsToken.value,
            title: jsExplanations[jsToken.value] || jsExplanations[jsToken.type]
        }))
    }
    const terms = result.filter(r => ['IdentifierName', 'Punctuator'].includes(r.type)).map(r => r.data)
    const mdn = mdnSearch(terms, 'js')
    return result.map(r => ({ ...r, mdn_path: mdn[r.data]?.url, title: r.title || mdn[r.data]?.title }))
}

function truncate_token(token) {
    if (['String', 'StringLiteral'].includes(token.type) && token.data.length > 35) {
        return {
            ...token,
            click_copy: token.data,
            data: token.data.slice(0, 20) + '💥' + token.data.slice(-15),
            title: 'String is truncated. Click to copy full value.\n\n' + token.title,
        }
    }
    return token
}

function mdnSearch(termsArr, lang) {
    const matches = {}
    const terms = [...new Set(termsArr)]
    for (const term of terms) {
        const termLow = term.toLowerCase()
        for (const result of mdnSearchIndex[lang]) {
            const lastPart = result.url.split('/').pop()
            if (lastPart === termLow || result.tags?.includes(term)) {
                matches[term] = result
                break
            }
        }
    }
    return matches
}

let mdnSearchIndex
async function initMdnSearchIndex() {
    if (mdnSearchIndex) return
    try {
        const request = await fetch('/mdn-search-index.json')
        if (!request.ok) {
            console.error('MDN search index fetch unsuccessful', request.statusText)
            return
        }
        const json = await request.json()
        mdnSearchIndex = json.reduce((acc, { title, url }) => {
            const lower = { title, url: url.toLowerCase() }
            if (url.includes('/Web/HTML/')) {
                acc.html.push(lower)
            } else if (url.includes('/Web/CSS/')) {
                acc.css.push(lower)
            } else if (
                ['Web/JavaScript/', 'Web/API'].some(term => url.includes(term)) &&
                !['Web/API/DOMException'].some(term => url.includes(term))
            ) {
                acc.js.push(lower)
            }
            return acc
        }, { html: [], css: [], js: [] })
        setCustomMdnEntries()
    } catch (e) {
        console.error('MDN search index fetch failed', e.message)
    }
}

function setCustomMdnEntries() {
    const customEntries = {
        html: [
            { title: '<h1>–<h6>: The HTML Section Heading elements', url: '/en-US/docs/Web/HTML/Element/Heading_Elements', tags: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] },
            { title: `target="_blank" makes the link open in a new tab. The syntax is strange because it's from the early days of HTML.`, url: '/en-US/docs/Web/HTML/Element/a#attr-target', tags: ['target'] },
            { title: "This is where you put the address (URL) where you want the link to take you.", url: '/en-US/docs/Web/HTML/Element/a#attr-href', tags: ['href'] },
        ],
        css: [
            { title: 'rgb()', url: '/en-US/docs/Web/CSS/color_value/rgb()', tags: ['rgb('] },
            { title: 'rgba()', url: '/en-US/docs/Web/CSS/color_value/rgba()', tags: ['rgba('] },
        ],
        js: [
            { title: '', url: '', tags: ['message', 'user', 'of', 'data'] },
            { title: 'Array. Put multiple values into a single variable.', url: '/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array', tags: ['[', ']'] },
            { title: 'A block statement is used to group zero or more statements.', url: '/en-US/docs/Web/JavaScript/Reference/Statements/block', tags: ['{', '}'] },
            { title: 'A for loop. There are 3 kinds of for loops. \n\n1. for statement, \n2. for..in statement \n3. for..of statement', url: '/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration', tags: ['for'] },

        ]
    }
    mdnSearchIndex = {
        html: customEntries.html.concat(mdnSearchIndex.html),
        css: customEntries.css.concat(mdnSearchIndex.css),
        js: customEntries.js.concat(mdnSearchIndex.js),
    }
}

export const HintLine = {
    async mounted() {
        await initMdnSearchIndex()
        const lang = this.el.getAttribute('data-lang')
        const tokens = tokenizeAndStandardise(this.el.innerText, lang)
        this.el.innerHTML = ''
        for (const token of tokens) {
            let el = document.createElement('a')
            el.title = token.title || ''
            if (token.mdn_path) {
                el.title += `${token.title ? '\n\n ' : ''}Click to read documentation on MDN`
                el.href = 'https://developer.mozilla.org' + token.mdn_path
                el.target = '_blank'
                el.classList.add('cursor-pointer', 'border-b-2', 'border-indigo-200', 'border-opacity-30', 'hover:border-opacity-80', 'hover:border-indigo-400')
            }
            if (token.click_copy) {
                el.addEventListener('click', () => navigator.clipboard.writeText(token.click_copy))
                el.classList.add('cursor-pointer')
            }
            el.innerText = token.data
            el.classList.add('hover:text-indigo-400')
            this.el.appendChild(el)
        }
    }
}
