export const CopyStudentLink = {
    mounted() {
        document.querySelector('#join-link-copy').onclick = e => {
            const text = document.querySelector('#join-link').innerText
            navigator.clipboard.writeText(text).then(() => { }, function (err) {
                console.error('Could not copy text: ', err)
            })
        }
    }
}
