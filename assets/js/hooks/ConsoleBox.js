const emojis = {
    warn: "⚠️",
    error: "🛑"
}

export const ConsoleBox = {
    populateConsole() {
        const projectWindow = document.querySelector('iframe#result-project').contentWindow
        const consoleEl = document.querySelector('#result-console')
        const lines = projectWindow.logLines || []
        projectWindow.logLines = []
        let hasError = false
        for (const { message, lineNumber, level } of lines) {
            if (level === "error") {
                hasError = true
            }
            let className = "text-gray-500"
            switch (level) {
                case "error": className = "text-red-500"; break;
                case "warn": className = "text-yellow-500"; break;
                case "info": className = "text-indigo-200"; break;
                case "log": className = "text-gray-200"; break;
                case "debug": className = "text-gray-500"; break;
            }
            consoleEl.innerHTML += `<p class="${className}"><span class="text-gray-500" title="Line number where message is from">[${`${lineNumber}`.padStart(3)}]</span> ${emojis[level] || "  "} ${message}</p>`
        }
        consoleEl.scrollTop = consoleEl.scrollHeight;
        if (hasError) {
            document.querySelector('#tab-console').classList.add('text-red-500')
        } else {
            document.querySelector('#tab-console').classList.remove('text-red-500')
        }
    },
    mounted() {
        const projectIframe = document.querySelector('iframe#result-project')

        projectIframe.contentWindow.updateUserConsole = () => {
            this.populateConsole()
        }
        projectIframe.contentWindow.clearConsole = function () {
            const consoleEl = document.querySelector('#result-console')
            consoleEl.innerText = ""
        }
        this.populateConsole()

        projectIframe.contentWindow.onbeforeunload = function () {
            projectIframe.onload = function () {
                this.mounted()
                const slowmo = document.querySelector('#slowmo-checkbox')
                projectIframe.contentWindow.steppingDelay = slowmo.checked ? 1000 : 0
            }
        }

        projectIframe.contentWindow.updateEditorHighlight = function (line, steppingDelay) {
            if (!steppingDelay || aceEditor.session.getMode().$id !== 'ace/mode/javascript') {
                return
            }
            const Range = ace.require('ace/range').Range
            const marker = aceEditor.session.addMarker(new Range(line, 0, line, 1), "ace-editor-highlight", "fullLine")
            setTimeout(() => aceEditor.session.removeMarker(marker), steppingDelay)
        }
    }
}
