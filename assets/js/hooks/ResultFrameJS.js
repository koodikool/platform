function getLineNumber(e) {
    const regex = /(Function|<anonymous>):(\d*):\d/
    let line

    if (e?.lineNumber) {
        line = e.lineNumber
    }

    if (e?.stack) {
        const match = e.stack.match(regex)
        line = match && match[2]
    }

    if (!line) {
        const match = (new Error()).stack.match(regex)
        line = match && match[2]
    }

    if (line) {
        line = Math.max(line - 2, 0)
    }

    return line || 'x'
}

export const ResultFrameJS = {
    mounted() {

        // Clear user set intervals
        window.userSetIntervalList = []
        const origInterval = window.setInterval
        window.setInterval = function (fn, delay) {
            window.userSetIntervalList.push(origInterval(fn, delay))
        }
        window.userSetTimeoutList = []
        const origTimeout = window.setTimeout
        window.setTimeout = function (fn, delay) {
            window.userSetTimeoutList.push(origTimeout(fn, delay))
        }

        // Overload console.*
        this.catchLog()

        // Display errors in UI console
        window.handleIframeError = function (e) {
            const lineNumber = getLineNumber(e)
            const message = e.message
            window.logLines.push({ lineNumber, message, level: "error" })
            if (window.updateUserConsole) {
                window.updateUserConsole()
            }
        }

        // Execute user's JavaScript
        this.execJS(this.el.dataset.content)
    },
    updated() {
        window.userSetIntervalList.forEach(interval => clearInterval(interval))
        window.userSetIntervalList = []
        window.userSetTimeoutList.forEach(timeout => clearTimeout(timeout))
        window.userSetTimeoutList = []
        this.execJS(this.el.dataset.content)
    },
    execJS(code) {
        if (window.clearConsole) {
            window.clearConsole()
        }
        try {
            Function(`"use strict"; ${code}`)()
        } catch (e) {
            handleIframeError(e)
        }
    },
    catchLog() {
        window.logLines = []

        window.onerror = function (message, url, lineNumber) {
            window.logLines.push({ message, lineNumber, level: "error" })
            if (window.updateUserConsole) {
                window.updateUserConsole()
            }
        }

        window.onunhandledrejection = function (error) {
            const message = `Uncaught Promise rejection: ${error.reason}`
            const lineNumber = 'x'
            window.logLines.push({ message, lineNumber, level: "error" })
            if (window.updateUserConsole) {
                window.updateUserConsole()
            }

        };

        const consoleKeys = ["error", "warn", "info", "log", "debug"]
        consoleKeys.forEach(level => {
            var baseLogFunction = window.console[level]
            window.console[level] = function () {
                baseLogFunction.apply(window.console, arguments)

                const message = Array.from(arguments).map(x => typeof x === "object" ? JSON.stringify(x) : x).join(', ')
                const lineNumber = getLineNumber()
                window.logLines.push({ message, lineNumber, level })
                if (window.updateUserConsole) {
                    window.updateUserConsole()
                }
            }
        })
    }
}
