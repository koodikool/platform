export const ChatBox = {
    mounted() {
        const msgBox = this.el.querySelector('#messages')
        msgBox.scrollTop = msgBox.scrollHeight - msgBox.clientHeight;
        document.querySelector('#btn-hide-chat').onclick = function (e) {
            e.target.parentElement.classList.toggle('translate-y-full')
            e.target.classList.toggle('-translate-y-full')
        }
        document.querySelectorAll('.empty-on-enter').forEach(el => {
            el.addEventListener('keyup', e => {
                if (e.key === "Enter") {
                    el.value = ''
                }
            })
        })
    },
    updated() {
        const msgBox = this.el.querySelector('#messages')
        msgBox.scrollTop = msgBox.scrollHeight - msgBox.clientHeight;
        while (msgBox.childElementCount > 100) {
            msgBox.removeChild(msgBox.childNodes[0])
        }
    }
}
