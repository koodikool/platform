export const ResultBox = {
    mounted() {
        document.querySelector('#refresh-tab').onclick = (e) => {
            document.getElementById("result-project").contentDocument.location.reload(true);
            // Hooks.ConsoleBox.mounted()
        }
        document.querySelector('#open-tab').onclick = (e) => {
            const url = document.getElementById("result-project").contentDocument.location
            window.open(url, '_blank')
        }
    }
}
