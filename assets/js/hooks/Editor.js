const beautifyOpts = {
    "indent_size": "4",
    "indent_char": " ",
    "max_preserve_newlines": "2",
    "preserve_newlines": true,
    "keep_array_indentation": false,
    "break_chained_methods": true,
    "indent_scripts": "normal",
    "brace_style": "collapse,preserve-inline",
    "space_before_conditional": true,
    "unescape_strings": false,
    "jslint_happy": false,
    "end_with_newline": false,
    "wrap_line_length": "80",
    "indent_inner_html": true,
    "comma_first": false,
    "e4x": false,
    "indent_empty_lines": false
}

export const Editor = {
    mounted() {
        this.handleEvent("points", ({ points }) => {
            new Audio('../sounds/harbour11__trumpet1.mp3').play()
        })

        // Start Ace editor
        ace.config.set('basePath', 'https://pagecdn.io/lib/ace/1.4.12/')
        window.aceEditor = ace.edit("code_input", {
            fontSize: 16,
            theme: "ace/theme/tomorrow_night",
            mode: "ace/mode/html",
            scrollPastEnd: 0.8,
            newLineMode: "unix",
            enableEmmet: true,
        })
        aceEditor.setKeyboardHandler('ace/keyboard/sublime');

        // Don't sync teacher updates
        if (this.el.getAttribute("data-teacher-view") === "true") {
            aceEditor.setReadOnly(true)
            return
        }

        // Send changes to server
        const debounceTimeout = parseInt(document.querySelector('input[type="hidden"]').value)
        let debounceTimer
        aceEditor.session.on('change', (_delta) => {
            if (debounceTimer) {
                clearTimeout(debounceTimer)
            }
            debounceTimer = setTimeout(() => {
                const name = document.querySelector('#editor-container').getAttribute("name")
                this.pushEvent("code-change", { [name]: aceEditor.getValue() })
            }, debounceTimeout)
        })
        aceEditor.session.on('changeMode', function (e, session) {
            if ('ace/mode/javascript' === session.getMode().$id) {
                if (!!session.$worker) {
                    session.$worker.send('changeOptions', [{
                        asi: true, // disable "Missing semicolon." warning in editor for JavaScript
                        esnext: false,
                        esversion: 9,
                    }])
                }
            }
        })

        document.querySelector('#slowmo-checkbox').onchange = function (e) {
            const projectIframe = document.querySelector('iframe#result-project')
            projectIframe.contentWindow.steppingDelay = this.checked ? 1000 : 0
        }
    },
    updated() {
        // Don't update code if tab hasn't changed
        const newLang1 = this.el.getAttribute('data-lang')
        const beautify = window[`${newLang1}_beautify`]
        const newLang = newLang1 === 'js' ? 'javascript' : newLang1
        const oldLang = aceEditor.session.getMode().$id.split('/')[2];
        if (newLang === oldLang && this.el.getAttribute("data-teacher-view") !== "true") {
            return
        }

        // Tab changed, change editor contents
        const code = beautify(this.el.innerText, beautifyOpts)
        aceEditor.setValue(code)
        aceEditor.gotoLine(9999999)
        aceEditor.session.setMode(`ace/mode/${newLang}`);
        aceEditor.session.$undoManager.reset()
    }
}
