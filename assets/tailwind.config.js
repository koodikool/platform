
module.exports = {
  mode: 'jit',
  purge: [
    './js/**/*.js',
    '../lib/*_web/**/*.*ex',
  ],
  theme: {
    extend: {
      gridTemplateRows: {
        'layout': 'repeat(3, 100%)',
        '3': '75px 1fr 1fr'
      },
      minWidth: {
        '1/4s': '25vw',
        '1/2s': '50vw',
        '3/4s': '75vw',
        '96': '24rem',
      },
      minHeight: {
        '1/4s': '25vh',
        '1/2s': '50vh',
        '3/4s': '75vh',
        '96': '24rem',
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            color: theme('colors.gray.100'),
            h1: {
              color: theme('colors.gray.100'),
            },
            h2: {
              color: theme('colors.gray.100'),
            },
            h3: {
              color: theme('colors.gray.100'),
            },
            th: {
              color: theme('colors.gray.100'),
            },
            'tbody tr, tr': {
              borderBottom: `1px solid ${theme('colors.gray.500')}`,
            },
            a: {
              color: theme('colors.gray.100'),
              '&:hover': {
                color: theme('colors.gray.100'),
              },
            },
          },
        },
      }),
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/typography'),
  ]
};
