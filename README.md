# Dev server

## Prerequisites

- Erlang v24
- Elixir v1.12.3
- PostgreSQL 12


**Quick commands (asdf & docker)**

```
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
asdf plugin-add nodejs
asdf install

docker run --name postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres:12
```

## Setup dev server

```
sudo mkdir /srv/tinkr-data
sudo chown krister:krister /srv/tinkr-data
mix deps.get
mix ecto.setup
npm install --prefix assets
```

## Run dev server

```
docker start postgres
mix phx.server
```

Then check out [`localhost:4000`](http://localhost:4000)

# Run tests

```
mix test
```

# Deploy to production

Just push to `production` branch and GitLab CI/CD will take care of it.

# Wiki

## How does SDB work? (Student Database)

Students can POST any JSON formatted data to `/sdb/:dn_name`. An agent is automatically created.

```
fetch('/sdb/any-name-you-like', {
    method: 'POST',
    headers: {'content-type': 'application/json'},
    body: JSON.stringify({a: 1, b: 2})
})
```

Then a simple GET request will get all of the contents.

```
fetch('/sdb/any-name-you-like')
    .then(response => response.json())
    .then(json => console.log(json))
```

The teacher (and student i guess) can delete databases with a browser by going to `tinkr.tech/sdb/any-name-you-like/drop`.

## How to update ProjectState model

1. Write a `migrate_snapshot` function in `platform/agents/project_state_agent.ex`.
1. Update the default version value in `platform/models/project_snapshot.ex`.
1. Update `platform/agents/project_state.ex` and make your changes.
