defmodule Platform.ProjectTemplateTest do
  use ExUnit.Case
  alias Platform.Models
  alias Platform.Models.Project
  alias Platform.Models.ProjectTemplate
  import Platform.Models.ProjectTemplate
  import Platform.MockData
  import Platform.SetupHelpers
  setup [:sandbox, :mock_project]
  doctest Platform.Models.ProjectTemplate
end
