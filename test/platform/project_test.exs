defmodule Platform.ProjectTest do
  use ExUnit.Case
  alias Platform.Models.Project
  alias Platform.Models.ProjectTemplate
  import Platform.Models.Project
  import Platform.SetupHelpers
  setup :sandbox
  doctest Platform.Models.Project
end
