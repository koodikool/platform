defmodule PlatformWeb.DayOfCodeTest do
  alias Platform.Models.DayOfCode
  use ExUnit.Case

  test "ExTwitter configuration", %{} do
    tweet = ExTwitter.lookup_status("1392720815058264065", trim_user: true) |> List.first()

    assert tweet.text ==
             "I'm a programmer looking to improve education. Please suggest me people to follow! The #edtech tag is crowded with boring newsletters."
  end

  test "Categorize 100DOC tweets", %{} do
    tweet_days =
      [
        {"1238894253981323264", 3},
        # This should actually be 53, but too low benefit/risk
        {"1238949029591031810", nil},
        {"1238996582969651200", 17},
        {"1239045661263200256", 4},
        {"1239059839046877186", 48},
        {"1239111364695724033", 1},
        {"1239172969903140864", 6},
        {"1239248608228081664", 59},
        {"1239254441364242435", 147},
        {"1239294198391746561", 3},
        {"1272552659183120385", 26},
        {"1272585412503056384", 88},
        {"1272593933185540097", nil},
        {"1272604461140148226", nil},
        {"1272624033650049030", 21},
        {"1272639741847130117", nil},
        {"1272670756988751873", 1}
      ]
      |> Enum.sort_by(&elem(&1, 0))

    parsed_days =
      tweet_days
      |> Enum.map(&elem(&1, 0))
      |> Enum.join(",")
      |> ExTwitter.lookup_status()
      |> Enum.sort_by(& &1.id_str)
      |> Enum.map(& &1.text)
      |> Enum.map(fn text -> DayOfCode.parse_day(text) end)

    assert parsed_days == Enum.map(tweet_days, &elem(&1, 1))
  end
end
