defmodule PlatformWeb.ProjectLessonsTest do
  alias Platform.Hints

  use ExUnit.Case

  test "Make HTML hint", %{} do
    diff = "
      +<div class=\"flex pane-padding\">
      +    <div class=\"vw33\">
      +        <img class=\"thumb-140\" src=\"data:image/svg+xml;base64\">
      +        <h1>Get a ride</h1>
      +        <p>Bolt offers you a ride in minutes.</p>
      +    </div>
      +    <div class=\"vw33\">
      +        <img class=\"thumb-140\" src=\"data:image/svg+xml;base64\">
      +        <h1>The best prices</h1>
      +        <p>We aim to offer the best ride prices in every city. See for yourself!</p>
      +    </div>
      +    <div class=\"vw33\">
      +        <img class=\"thumb-140\" src=\"data:image/svg+xml;base64\">
      +        <h1>Easy to use</h1>
      +        <p>Get wherever you need to go as quickly as possible.</p>
      +    </div>
      +</div>

      +<link rel=\"stylesheet\" href=\"style.css\">
      +<script src=\"script.js\"></script>
    "

    result = Hints.make_hints(diff, :html)

    assert result ==
             [
               [
                 "+<div class=\"flex pane-padding\">Change me</div>",
                 "+<div class=\"vw33\">Change me</div>",
                 "+<h1>Change me</h1>",
                 "+<img class=\"thumb-140\" src=\"data:image/svg+xml;base64\">",
                 "+<p>Change me</p>"
               ],
               [
                 "      +<div class=\"flex pane-padding\">",
                 "      +    <div class=\"vw33\">",
                 "      +        <img class=\"thumb-140\" src=\"data:image/svg+xml;base64\">",
                 "      +        <h1>Get a ride</h1>",
                 "      +        <p>Bolt offers you a ride in minutes.</p>",
                 "      +    </div>",
                 "      +    <div class=\"vw33\">",
                 "      +        <img class=\"thumb-140\" src=\"data:image/svg+xml;base64\">",
                 "      +        <h1>The best prices</h1>",
                 "      +        <p>We aim to offer the best ride prices in every city. See for yourself!</p>",
                 "      +    </div>",
                 "      +    <div class=\"vw33\">",
                 "      +        <img class=\"thumb-140\" src=\"data:image/svg+xml;base64\">",
                 "      +        <h1>Easy to use</h1>",
                 "      +        <p>Get wherever you need to go as quickly as possible.</p>",
                 "      +    </div>",
                 "      +</div>"
               ]
             ]
  end

  test "Make CSS hint", %{} do
    diff = "
      +.vw33 {
      +    width: 33vw;
      +}
      +
      .pane-padding {
          padding: 20px;
      }
      @@ -84,3 +88,9 @@ p.large {
          border-radius: 100px;
          margin: 30px 0px
      }
      +
      +.thumb-140 {
      +    height: 140px;
      +    width: 140px;
      +    vertical-align: bottom;
      +}
    "

    result = Hints.make_hints(diff, :css)

    assert result == [
             [
               "      +    height: 140px;",
               "      +    vertical-align: bottom;",
               "      +    width: 140px;",
               "      +    width: 33vw;",
               "      +.thumb-140 {",
               "      +.vw33 {",
               "      +}",
               "      +}"
             ],
             [
               "      +.vw33 {",
               "      +    width: 33vw;",
               "      +}",
               "      +.thumb-140 {",
               "      +    height: 140px;",
               "      +    width: 140px;",
               "      +    vertical-align: bottom;",
               "      +}"
             ]
           ]
  end
end
