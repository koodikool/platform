defmodule PlatformWeb.UsecasesTest do
  use ExUnit.Case, async: false
  use Wallaby.Feature
  import Wallaby.Query
  import Platform.MockData

  @username "Tester1"

  setup :mock_project

  feature "student starts a project and writes code", %{session: student} do
    student
    |> resize_window(1600, 768)
    |> visit("/learn")
    |> take_screenshot()
    |> click(button("Start this project"))
    |> click(button("Start building this project"))
    |> fill_in(text_field("user_name"), with: @username)
    |> click(button("Next"))
    |> assert_has(Query.text(@username))
    |> assert_has(Query.text("Ask for help"))
    |> assert_has(Query.css("#code_input"))
    |> click(Query.css("#code_input"))
    |> send_keys(["<h1>Tere<h1>"])
    |> execute_script(
      "return document.querySelector('iframe').contentWindow.document.querySelector('h1').innerText",
      [],
      fn result -> assert result == "Tere" end
    )
    |> click(button("CSS"))
    |> click(Query.css("#code_input"))
    |> send_keys(["body {background-color: red}"])
    |> execute_script(
      "return window.getComputedStyle(document.querySelector('iframe').contentWindow.document.body).backgroundColor",
      [],
      fn result -> assert result == "rgb(255, 0, 0)" end
    )
    |> click(button("JavaScript"))
    |> click(Query.css("#code_input"))
    |> send_keys([
      "const p = document.createElement('p'); p.id='glxiuh'; p.innerText = 'mytext'; document.body.appendChild(p);"
    ])
    |> execute_script(
      "return document.querySelector('iframe').contentWindow.document.querySelector('#glxiuh').innerText",
      [],
      fn result -> assert result == "mytext" end
    )
  end

  @sessions 2
  feature "student completes 2 tasks in a room", %{sessions: [teacher, student]} do
    teacher
    |> visit("/teach")
    |> click(button("Create new room"))
    |> fill_in(text_field("user_name"), with: "Teacher Tester")
    |> click(button("Next"))
    |> fill_in(text_field("room_name"), with: "Tuuba")
    |> click(option("github/Koodikool/chat-app"))
    |> click(button("Create room"))

    join_link = Browser.text(teacher, Query.css("#join-link"))

    student
    |> visit(join_link)
    |> fill_in(text_field("user_name"), with: @username)
    |> click(button("Next"))
    |> click(button("Start building this project"))
    |> click(button("Next task (1 / 33)"))

    teacher
    |> click(Query.text(@username))
    |> click(Query.text("Accept"))
    |> click(Query.text("Free flow"))

    student
    |> click(Query.text("Next >"))
    |> assert_text("Find the Developer Console. If there are errors, they will show there.")
  end

  feature "student returns to existing project", %{session: student} do
    student
    |> visit("/learn")
    |> click(button("Start this project"))
    |> click(button("Start building this project"))
    |> fill_in(text_field("user_name"), with: @username)
    |> click(button("Next"))
    |> assert_has(Query.text(@username))
    |> visit("/learn")
    |> click(button("Continue project"))
  end

  @sessions 2
  feature "student and teacher chat", %{sessions: [teacher, student]} do
    teacher
    |> visit("/teach")
    |> click(button("Create new room"))
    |> fill_in(text_field("user_name"), with: "Teacher Tester")
    |> click(button("Next"))
    |> fill_in(text_field("room_name"), with: "Tuuba")
    |> click(option("github/Koodikool/chat-app"))
    |> click(button("Create room"))

    join_link = Browser.text(teacher, Query.css("#join-link"))

    student
    |> visit(join_link)
    |> fill_in(text_field("user_name"), with: @username)
    |> click(button("Next"))
    |> click(button("Start building this project"))
    |> fill_in(text_field("Write message"),
      with: "is the chat working"
    )
    |> send_keys([:enter])

    teacher
    |> click(Query.text(@username))
    |> assert_has(Query.text("is the chat working"))
    |> assert_has(Query.text("💬"))
    |> click(Query.text("is the chat working"))
    |> refute_has(Query.text("💬"))
  end

  feature "student console", %{session: student} do
    student
    |> resize_window(1600, 768)
    |> visit("/learn")
    |> click(button("Start this project"))
    |> click(button("Start building this project"))
    |> fill_in(text_field("user_name"), with: @username)
    |> click(button("Next"))
    |> assert_has(Query.text(@username))
    |> click(button("JavaScript"))
    |> click(Query.css("#code_input"))
    |> send_keys(["console.log('testing the console')"])
    |> click(Query.css("#tab-console"))
    |> assert_has(Query.css("#result-console") |> Query.text("[  1]    testing the console"))
  end

  feature "student gets Free Flow by default", %{session: student} do
    student
    |> visit("/learn")
    |> click(button("Start this project"))
    |> click(button("Start building this project"))
    |> fill_in(text_field("user_name"), with: @username)
    |> click(button("Next"))
    |> assert_has(Query.text(@username))
    |> assert_has(Query.text("Free flow"))
  end

  @sessions 2
  feature "Room may enable Free Flow for all", %{sessions: [teacher, student]} do
    teacher
    |> visit("/teach")
    |> click(button("Create new room"))
    |> fill_in(text_field("user_name"), with: "Teacher Tester")
    |> click(button("Next"))
    |> fill_in(text_field("room_name"), with: "Tuuba")
    |> click(option("github/Koodikool/chat-app"))
    |> click(Query.css("#room_free_flow"))
    |> click(button("Create room"))

    join_link = Browser.text(teacher, Query.css("#join-link"))

    student
    |> visit(join_link)
    |> fill_in(text_field("user_name"), with: @username)
    |> click(button("Next"))
    |> click(button("Start building this project"))
    |> assert_has(Query.text("Free flow"))
  end

  feature "Student registers to save progress", %{session: student} do
    student
    |> visit("/learn")
    |> click(button("Start this project"))
    |> click(button("Start building this project"))
    |> fill_in(text_field("user_name"), with: @username)
    |> click(button("Next"))
    |> assert_has(Query.text(@username))
    |> click(button("Sign up"))
    |> fill_in(text_field("user_email"), with: "test@example.com")
    |> fill_in(text_field("user_password"), with: "123456789012")
    |> click(button("Submit"))
    |> click(Query.text("Log out"))
    |> visit("/learn")
    |> click(button("Log in"))
    |> fill_in(text_field("user_email"), with: "test@example.com")
    |> fill_in(text_field("user_password"), with: "123456789012")
    |> click(button("Submit"))
    |> assert_has(Query.text("Continue project"))
  end
end
