defmodule PlatformWeb.MissionLoadTest do
  use PlatformWeb.ConnCase
  import Phoenix.LiveViewTest
  import Platform.MockData
  setup :mock_project

  test "load for 100 students", %{conn: conn, mock_data: %{project_template: project_template}} do
    project_slug_path =
      Routes.student_path(conn, :init_project, String.split(project_template.slug, "/"))

    time_start = System.monotonic_time(:millisecond)

    project_paths =
      for _i <- 1..100 do
        conn =
          post(
            conn,
            Routes.user_creation_path(conn, :create, %{
              "user" => %{"name" => "Tester"}
            })
          )
          |> recycle()

        conn_parsed = get(conn, project_slug_path)

        with %{resp_headers: resp_headers} = conn_parsed do
          Enum.find_value(resp_headers, fn
            {"location", path} -> {conn, path}
            {_, _} -> false
          end)
        end
      end

    time_create_projects = System.monotonic_time(:millisecond)
    assert time_create_projects - time_start < 800

    for {user_conn, project_path} <- project_paths do
      {:ok, view, _html} = live(user_conn, project_path)

      assert view
             |> element("h2", "Create a space for showing messages and writing")
             |> has_element?()
    end

    time_first_request = System.monotonic_time(:millisecond)
    assert time_first_request - time_create_projects < 5000

    for {user_conn, project_path} <- project_paths do
      {:ok, view, _html} = live(user_conn, project_path)

      assert view
             |> element("h2", "Create a space for showing messages and writing")
             |> has_element?()
    end

    time_second_request = System.monotonic_time(:millisecond)
    assert time_second_request - time_first_request < 2000
  end
end
