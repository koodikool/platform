defmodule Platform.StudentControllerTest do
  use PlatformWeb.ConnCase
  import Platform.MockData
  import Platform.SetupHelpers

  setup [:sandbox, :mock_course_projects]

  describe "list_projects" do
    test "list all projects", %{conn: conn} do
      conn =
        post(conn, Routes.user_creation_path(conn, :create, %{"user" => %{"name" => "Tester"}}))

      get(conn, Routes.student_path(conn, :init_project, ["github", "Koodikool", "chat-app"]))

      conn = get(conn, Routes.student_path(conn, :list_projects))

      html = html_response(conn, 200)

      assert html =~ "Template2"
      assert html =~ "Course2"

      [_start, course1, course2, no_course] = String.split(html, "data-course")

      assert course1 =~ "Template2"
      assert course1 =~ "Template4"
      assert course2 =~ "Template2"
      assert course2 =~ "Template3"
      assert course2 =~ "Template5"
      assert no_course =~ "Template1"
      assert no_course =~ "Template6"
      assert html =~ "Continue"

      count_projects = String.split(html, "Continue") |> length() |> then(&(&1 - 1))
      assert count_projects == 1
    end
  end
end
