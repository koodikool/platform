defmodule Platform.StudentDBControllerTest do
  use PlatformWeb.ConnCase

  test "insert a thing - then fetch", %{conn: conn} do
    data = %{"stuff" => "in here"}

    conn = post(conn, Routes.student_db_path(conn, :push, "test_db"), data)
    assert 201 == conn.status

    conn = get(conn, Routes.student_db_path(conn, :get, "test_db"))
    assert [%{"stuff" => "in here", "id" => _id}] = json_response(conn, 200)
  end

  test "Fail with 'id' key", %{conn: conn} do
    data = %{"id" => "asd", "stuff" => "in here"}
    conn = post(conn, Routes.student_db_path(conn, :push, "test_db"), data)
    assert 400 == conn.status
  end

  test "Get messages since", %{conn: conn} do
    for i <- 1..10 do
      data = %{"stuff" => i}
      conn = post(conn, Routes.student_db_path(conn, :push, "messages"), data)
      assert 201 == conn.status
    end

    conn = get(conn, Routes.student_db_path(conn, :get, "messages"))
    json = json_response(conn, 200)
    assert 10 == length(json)

    since_id = json |> Enum.at(7) |> Map.get("id")
    conn = get(conn, Routes.student_db_path(conn, :get, "messages", since_id))
    json = json_response(conn, 200)
    assert 2 == length(json)
    assert 9 == json |> List.first() |> Map.get("stuff")
  end
end
