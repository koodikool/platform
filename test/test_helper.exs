ExUnit.start()
Code.require_file("support/mock_data.exs", "test/")
Code.require_file("support/setup_helpers.exs", "test/")
Ecto.Adapters.SQL.Sandbox.mode(Platform.Repo, {:shared, self()})
Application.put_env(:wallaby, :base_url, PlatformWeb.Endpoint.url())
{:ok, _} = Application.ensure_all_started(:wallaby)
