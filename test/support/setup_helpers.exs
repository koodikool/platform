defmodule Platform.SetupHelpers do
  def sandbox(_context \\ nil) do
    # Explicitly get a connection before each test
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Platform.Repo)
  end
end
