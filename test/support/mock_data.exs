defmodule Platform.MockData do
  alias Platform.Models
  alias Platform.Accounts
  require Ecto.Query
  use ExUnit.Case
  use Plug.Test

  def mock_project(_context \\ nil) do
    {:ok, user} = Accounts.create_user(%{name: "Teacher Tester"})

    {:ok, project_template} =
      Models.create_project_template(%{
        description: "1",
        git_repo: "https://github.com/Koodikool/chat-app.git",
        public: true,
        title: "Chat App",
        created_by: user.id
      })

    %{
      mock_data: %{
        user: user,
        project_template: project_template
      }
    }
  end

  def mock_course_projects(_context \\ nil) do
    {:ok, user1} = Accounts.create_user(%{name: "Tester 1"})
    {:ok, user2} = Accounts.create_user(%{name: "Tester 2"})

    templates =
      for i <- 1..5 do
        {:ok, template} =
          Models.create_project_template(%{
            description: "#{i}",
            git_repo: "https://github.com/Koodikool/chat-app-#{i}.git",
            public: true,
            title: "Template#{i}",
            created_by: user1.id
          })

        template
      end

    {:ok, template6} =
      Models.create_project_template(%{
        description: "6",
        git_repo: "https://github.com/Koodikool/chat-app.git",
        public: true,
        title: "Template6",
        created_by: user2.id
      })

    projects =
      for i <- 1..3 do
        {:ok, project} =
          Models.create_project(%{
            free_flow: true,
            created_by: user1.id,
            project_template_id: Enum.at(templates, i) |> Map.get(:id)
          })

        project
      end

    {:ok, course1} =
      Models.create_course(%{
        title: "Course1",
        project_templates: templates |> Enum.drop_every(2) |> Enum.map(& &1.id)
      })

    {:ok, course2} =
      Models.create_course(%{
        title: "Course2",
        project_templates: templates |> Enum.drop_every(3) |> Enum.map(& &1.id)
      })

    %{
      templates: templates ++ [template6],
      projects: projects,
      courses: [course1, course2]
    }
  end
end
