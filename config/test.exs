use Mix.Config

# Only in tests, remove the complexity from the password hashing algorithm
config :bcrypt_elixir, :log_rounds, 1

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :platform, Platform.Repo,
  username: "postgres",
  password: "postgres",
  database: "platform_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: System.get_env("DB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :platform, PlatformWeb.Endpoint,
  http: [port: 4002],
  server: true

config :platform, :sandbox, Ecto.Adapters.SQL.Sandbox

# Print only warnings and errors during test
config :logger, level: :warn

config :platform, :basic_auth, username: "asd", password: "asd"

config :platform, Platform.StudentDBAgent,
  student_db_size_limit: 100,
  ratelimits: %{period: 10000, count: 500}

config :platform, :config, editor_debounce: 0

config :hammer,
  backend: {Hammer.Backend.ETS, [expiry_ms: 60_000 * 60 * 4, cleanup_interval_ms: 60_000 * 10]}

config :wallaby,
  otp_app: :platform,
  chromedriver: [
    capabilities: %{
      javascriptEnabled: true,
      chromeOptions: %{
        args: [
          "--no-sandbox",
          "--disable-gpu",
          "--headless"
        ]
      }
    }
  ]

import_config "secret.exs"
