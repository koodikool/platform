defmodule PlatformWeb.TeacherMissionLive do
  use PlatformWeb, :live_view
  import Destructure
  alias Phoenix.PubSub
  alias Platform.Mission
  alias Platform.ProjectState
  alias Platform.ProjectStateAgent
  alias Platform.RoomOverview
  alias Platform.Accounts

  @impl true
  def mount(s(%{room_id}) = params, %{"user_token" => user_token}, socket) do
    %{id: user_id} = Accounts.get_user_by_session_token(user_token)
    PubSub.subscribe(Platform.PubSub, "room_update:#{room_id}")

    project =
      with project_id when is_bitstring(project_id) <- Map.get(params, "project_id") do
        ProjectStateAgent.update(project_id, :has_updates, false)
        Mission.get(project_id)
      end

    {:ok,
     assign(socket,
       join_url: Routes.url(socket) <> Routes.student_path(socket, :join_room, room_id),
       room_id: room_id,
       room_overview: RoomOverview.get(room_id),
       project: project,
       user_id: user_id,
       tabs: %{:code_tab => :html, :result_tab => :users}
     )}
  end

  @impl true
  def mount(s(%{project_id}), %{"user_token" => user_token}, socket) do
    %{id: user_id} = Accounts.get_user_by_session_token(user_token)
    PubSub.subscribe(Platform.PubSub, "project_update:#{project_id}")
    ProjectStateAgent.update(project_id, :has_updates, false)

    project = Mission.get(project_id)

    {:ok,
     assign(socket,
       join_url: nil,
       room_id: nil,
       room_overview: nil,
       project: project,
       user_id: user_id,
       tabs: %{:code_tab => :html, :result_tab => :users}
     )}
  end

  @impl true
  def handle_info({:project_update, %ProjectState{} = new_project_state}, socket) do
    d(%{project, room_id}) = socket.assigns

    project =
      case not is_nil(project) and project.project_id == new_project_state.project_id do
        false -> project
        true -> Mission.get(new_project_state)
      end

    room_overview =
      case room_id do
        nil -> nil
        room_id -> RoomOverview.get(room_id)
      end

    {:noreply,
     assign(socket,
       project: project,
       room_overview: room_overview
     )}
  end

  @impl true
  def handle_event("next_lesson", %{"value" => "false"}, socket) do
    d(%{project_id}) = socket.assigns.project
    ProjectStateAgent.update(project_id, :ready_next, false)
    {:noreply, socket}
  end

  @impl true
  def handle_event("next_lesson", %{"value" => lesson_hash}, socket) do
    d(%{project_id}) = socket.assigns.project
    {:ok, _snapshot} = Mission.next_lesson(project_id, lesson_hash)
    {:noreply, socket}
  end

  @impl true
  def handle_event("help_arrived", _value, socket) do
    d(%{project_id}) = socket.assigns.project
    ProjectStateAgent.update(project_id, :asking_help, false)
    {:noreply, socket}
  end

  @impl true
  def handle_event("free_flow", _value, socket) do
    d(%{project_id, free_flow}) = socket.assigns.project
    ProjectStateAgent.update(project_id, :free_flow, !free_flow)
    {:noreply, socket}
  end

  @impl true
  def handle_event("change-tab", new_tab, socket) do
    {tab_type, tab_value} =
      case new_tab do
        %{"code_tab" => "html"} -> {:code_tab, :html}
        %{"code_tab" => "css"} -> {:code_tab, :css}
        %{"code_tab" => "js"} -> {:code_tab, :js}
        %{"result_tab" => "users"} -> {:result_tab, :users}
        %{"result_tab" => "goal"} -> {:result_tab, :goal}
        %{"result_tab" => "console"} -> {:result_tab, :console}
      end

    {:noreply, assign(socket, tabs: Map.put(socket.assigns.tabs, tab_type, tab_value))}
  end

  @impl true
  def handle_event("next-hint", %{"value" => _filetype}, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("next-lesson", %{"value" => lesson_hash}, socket) do
    d(%{project_id}) = socket.assigns.project
    {:ok, _snapshot} = Mission.next_lesson(project_id, lesson_hash)

    {:noreply, socket}
  end

  @impl true
  def handle_event("prev-lesson", %{"value" => lesson_hash}, socket) do
    d(%{project_id}) = socket.assigns.project
    {:ok, _snapshot} = Mission.previous_lesson(project_id, lesson_hash)

    {:noreply, socket}
  end

  @impl true
  def handle_event("completed", _value, socket) do
    d(%{project_id, completed}) = socket.assigns.project
    ProjectStateAgent.update(project_id, :completed, !completed)
    {:noreply, socket}
  end
end
