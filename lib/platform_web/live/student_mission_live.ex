defmodule PlatformWeb.StudentMissionLive do
  alias Phoenix.PubSub
  alias Platform.ProjectState
  alias Platform.ProjectStateAgent
  alias Platform.Mission
  alias Platform.Accounts
  use PlatformWeb, :live_view
  import Destructure

  @impl true
  def mount(%{"project_id" => project_id}, %{"user_token" => user_token}, socket) do
    %{id: user_id} = Accounts.get_user_by_session_token(user_token)
    PubSub.subscribe(Platform.PubSub, "project_update:#{project_id}")

    # Don't run timer on server render, only client
    if socket.root_pid do
      Task.start(fn -> Mission.hint_timer(project_id) end)
    end

    {:ok,
     assign(socket,
       project: Mission.get(project_id),
       user_id: user_id,
       tabs: %{:code_tab => :html, :result_tab => :users}
     )}
  end

  @impl true
  def handle_info({:project_update, %ProjectState{} = new_project_state}, socket) do
    {:noreply, assign(socket, project: Mission.get(new_project_state))}
  end

  @impl true
  def handle_info(:next_task, socket) do
    {:noreply, push_event(socket, "points", %{})}
  end

  @impl true
  def handle_event("code-change", new_code, socket) do
    d(%{code, project_id}) = socket.assigns.project

    {key, content} =
      case new_code do
        %{"html" => content} -> {:html, content}
        %{"css" => content} -> {:css, content}
        %{"js" => content} -> {:js, content}
      end

    ProjectStateAgent.update(project_id, :code, Map.put(code, key, content))

    {:noreply, socket}
  end

  @impl true
  def handle_event("next-lesson", %{"value" => lesson_hash}, socket) do
    d(%{project_id, ready_next, free_flow}) = socket.assigns.project

    case free_flow do
      true ->
        {:ok, _snapshot} = Mission.next_lesson(project_id, lesson_hash)

      false ->
        ProjectStateAgent.update(project_id, :ready_next, !ready_next)
    end

    {:noreply, socket}
  end

  @impl true
  def handle_event("prev-lesson", %{"value" => lesson_hash}, socket) do
    d(%{project_id, free_flow}) = socket.assigns.project

    case free_flow do
      true ->
        {:ok, _snapshot} = Mission.previous_lesson(project_id, lesson_hash)
    end

    {:noreply, socket}
  end

  @impl true
  def handle_event("ask-help", _values, socket) do
    d(%{project_id, asking_help}) = socket.assigns.project
    ProjectStateAgent.update(project_id, :asking_help, !asking_help)
    {:noreply, socket}
  end

  @impl true
  def handle_event("change-tab", new_tab, socket) do
    {tab_type, tab_value} =
      case new_tab do
        %{"code_tab" => "html"} -> {:code_tab, :html}
        %{"code_tab" => "css"} -> {:code_tab, :css}
        %{"code_tab" => "js"} -> {:code_tab, :js}
        %{"result_tab" => "users"} -> {:result_tab, :users}
        %{"result_tab" => "goal"} -> {:result_tab, :goal}
        %{"result_tab" => "console"} -> {:result_tab, :console}
      end

    {:noreply, assign(socket, tabs: Map.put(socket.assigns.tabs, tab_type, tab_value))}
  end

  @impl true
  def handle_event("next-hint", %{"value" => filetype}, socket) do
    d(%{hints, project_id}) = socket.assigns.project

    {key, new_count} =
      case filetype do
        "html" -> {:html, Map.get(hints, :html) + 1}
        "css" -> {:css, Map.get(hints, :css) + 1}
        "js" -> {:js, Map.get(hints, :js) + 1}
      end

    ProjectStateAgent.update(project_id, :hints, Map.put(hints, key, new_count))

    Task.start(fn -> Mission.hint_timer(project_id) end)

    {:noreply, socket}
  end
end
