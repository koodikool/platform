<div class="flex flex-col h-screen">
  <%= if @join_url do %>
    <div class="flex items-baseline">
      <a href="/teach"><h3 class="px-4 cursor-pointer opacity-50">🏠 Home</h3></a>
      <p>
      Your students can join using this link:
        <code class="bg-gray-900 px-2 py-1 text-gray-300 rounded" id="join-link">
          <%= @join_url %>
        </code>
        <button id="join-link-copy" phx-hook="CopyStudentLink">Copy</button>
      </p>
    </div>

  <% end %>

  <%= if not is_nil(@room_overview) and length(@room_overview) == 0 do %>
    <div class="flex-grow justify-center flex">
      <h1 class="self-center"><%= gettext("No students joined yet") %></h1>
    </div>
  <% else %>
    <%= if @room_overview do %>
      <%= live_component PlatformWeb.RoomOverviewComponent, projects: @room_overview, active_project_id: @project && @project.project_id %>
    <% end %>

    <div class="p-5 flex flex-col flex-grow justify-center">
        <%= if is_nil(@project) do %>
          <h1 class="self-center"><%= gettext("Select a student to display") %></h1>
        <% else %>
          <%= live_render @socket, PlatformWeb.ChatComponent, id: @project.project_id, session: %{"parent_type"=> "project", "parent_id"=> @project.project_id, "from"=> @user_id} %>
          <h1 class="text-center  text-gray-400">
            <%= @project.username %>
            <a class="text-base align-top" href={"/project/#{ @project.project_id }"} target="_blank">
              <button class="px-1 py-0 bg-transparent border opacity-50 hover:opacity-100 hover:bg-transparent m-0"
                title="Open student learning environment in new tab">↥</button>
            </a>
            <a class="text-base align-top" href={Routes.project_path(@socket, :edit, @project.project_id)} target="_blank">
              <button class="px-1 py-0 bg-transparent border opacity-50 hover:opacity-100 hover:bg-transparent m-0"
                title="Open student learning environment in new tab">🖉</button>
            </a>
          </h1>
          <div class="flex justify-between flex-col md:flex-row">
            <div>
              <%= if @project.completed do %>
                <h1 class="text-center m-4"><%= gettext("All tasks are completed!") %></h1>
                <h2 class="text-center m-4"><%= gettext("Please leave a testimonial with @tinkr_tech on twitter, it helps out a lot.") %></h2>
              <% else %>
                <h2 class="text-center m-4"><%= @project.lesson.title %> (<%= @project.lesson.progressStr %>)</h2>
              <% end %>
            </div>
            <div class="flex flex-col items-end">
              <div class="flex">
                <%= if @project.completed do %>
                  <button class="shadow-sm bg-green-700 text-green-100 hover:bg-green-600 m-auto block min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" phx-click="completed">
                    <h3><%= gettext("Completed") %></h3>
                  </button>
                <% else %>
                  <button class="shadow-sm bg-gray-900 text-gray-400 hover:bg-green-600 hover:text-gray-100 m-auto block min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" phx-click="completed">
                    <h3><%= gettext("Completed") %></h3>
                  </button>
                <% end %>
                <div>
                  <h3 class="text-green-400  text-center">
                    Lesson <%= @project.lesson.progressStr %>
                  </h3>
                  <div class="flex">
                    <button class="px-6 py-2 bg-transparent border border-gray-500" phx-click="prev-lesson" value={ @project.lesson.hash }> &lt; Back </button>
                    <button class="px-6 py-2 bg-green-500 hover:bg-green-400" phx-click="next-lesson" value={ @project.lesson.hash }> Next &gt; </button>
                  </div>
                </div>
              </div>
              <div class="flex justify-center flex-col xl:flex-row">
                <%= if @project.ready_next do %>
                  <div class="flex">
                      <button class="shadow-sm bg-green-700 text-green-100 hover:bg-green-600 m-auto min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" phx-click="next_lesson" value={ @project.lesson.hash }>
                          <h3><%= gettext("Accept") %></h3>
                      </button>
                      <button class="shadow-sm bg-red-900 text-red-100 hover:bg-red-800 m-auto min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" phx-click="next_lesson" value="false">
                          <h3><%= gettext("Reject") %></h3>
                      </button>
                  </div>
                <% else %>
                  <div class="flex">
                      <button class="shadow-sm bg-green-700 text-green-100 m-auto min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" disabled>
                          <h3><%= gettext("Accept") %></h3>
                      </button>
                      <button class="shadow-sm bg-red-900 text-red-100 m-auto min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" disabled>
                          <h3><%= gettext("Reject") %></h3>
                      </button>
                  </div>
                <% end %>
                <%= if @project.asking_help do %>
                  <button class="shadow-sm bg-green-700 text-green-100 hover:bg-green-600 m-auto block min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" phx-click="help_arrived">
                    <h3><%= gettext("The student has been helped") %></h3>
                  </button>
                <% else %>
                  <button class="shadow-sm bg-green-700 text-green-100 m-auto block min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" disabled>
                    <h3><%= gettext("The student has been helped") %></h3>
                  </button>
                <% end %>
                <%= if @project.free_flow do %>
                  <button class="shadow-sm bg-green-700 text-green-100 hover:bg-green-600 m-auto block min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" phx-click="free_flow">
                    <h3><%= gettext("Free flow") %></h3>
                  </button>
                <% else %>
                  <button class="shadow-sm bg-gray-900 text-gray-400 hover:bg-green-600 hover:text-gray-100 m-auto block min-w-min p-3 whitespace-nowrap xl:mx-2 xl:w-1/4" phx-click="free_flow">
                    <h3><%= gettext("Free flow") %></h3>
                  </button>
                <% end %>
              </div>
            </div>
          </div>
          <%= live_component PlatformWeb.MissionComponent, project: @project, teacher_view: true, tabs: @tabs %>
        <% end %>
    </div>
  <% end %>
</div>
