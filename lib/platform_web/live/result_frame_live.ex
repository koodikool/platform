defmodule PlatformWeb.ResultFrameLive do
  use PlatformWeb, :live_view_result
  import Destructure
  alias Phoenix.PubSub
  alias Platform.ProjectLessons
  alias Platform.ProjectStateAgent
  alias Platform.Accounts

  def mount(s(%{"type" => "project", project_id}), _socket, socket) do
    PubSub.subscribe(Platform.PubSub, "project_update:#{project_id}")
    project = ProjectStateAgent.get(project_id)
    lesson = ProjectLessons.get_lesson(project.project_template_id, project.lesson_hash)

    bgjs =
      case Map.get(lesson.files, :backgroundjs) do
        nil -> nil
        file -> Map.get(file, :content_end)
      end

    {:ok, assign(socket, code: project.code, background_js: bgjs)}
  end

  def mount(s(%{"type" => "template", project_template_id, lesson_hash}), _socket, socket) do
    files =
      project_template_id
      |> ProjectLessons.get_lesson(lesson_hash)
      |> Map.get(:files)
      |> Enum.map(fn {ext, f} -> {ext, f.content_end} end)
      |> Map.new()

    bgjs = Map.get(files, :backgroundjs)
    {:ok, assign(socket, code: files, background_js: bgjs)}
  end

  def mount(s(%{"type" => "template", project_id}), _socket, socket) do
    files =
      project_id
      |> ProjectLessons.get()
      |> List.last()
      |> Map.get(:files)
      |> Enum.map(fn {ext, f} -> {ext, f.content_end} end)
      |> Map.new()

    bgjs = Map.get(files, :backgroundjs)
    {:ok, assign(socket, code: files, background_js: bgjs)}
  end

  @doc """
  Temporary function to gather data about bug
  """
  def mount(params, s(%{user_token}), _socket) do
    user = Accounts.get_user_by_session_token(user_token)
    IO.inspect(params, label: "params")
    IO.inspect(user, label: "user")
    throw("Result Frame Live mount params wrong")
  end

  def handle_info({:project_update, project}, socket) do
    {:noreply, assign(socket, code: project.code)}
  end
end
