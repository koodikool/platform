defmodule PlatformWeb.Router do
  use PlatformWeb, :router
  import PlatformWeb.UserAuth
  import Plug.BasicAuth
  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {PlatformWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :result_layout do
    plug :put_root_layout, {PlatformWeb.LayoutView, :result}
  end

  pipeline :home_layout do
    plug :put_root_layout, {PlatformWeb.LayoutView, :home}
  end

  pipeline :empty_tailwind_layout do
    plug :put_root_layout, {PlatformWeb.LayoutView, :empty_tailwind}
  end

  pipeline :admin_auth do
    plug :basic_auth, Application.compile_env(:platform, :basic_auth)
  end

  pipeline :api do
    plug :fetch_session
    plug :accepts, ["json"]
  end

  scope "/", PlatformWeb do
    pipe_through [:browser, :home_layout]
    get "/100daysofcode-stats", DaysOfCodeController, :index
    get "/mdn-search-index.json", StudentController, :mdn_search_index

    for path <- PlatformWeb.HomeController.available_paths() do
      get "/#{path}", HomeController, :static_page_abtest
    end
  end

  scope "/unlisted", PlatformWeb do
    pipe_through [:browser, :empty_tailwind_layout]

    get "/landing_1", HomeController, :static_page
  end

  scope "/learn", PlatformWeb do
    pipe_through [:browser, :migrate_old_user]
    get "/", StudentController, :list_projects

    pipe_through :require_authenticated_user
    get "/room/exit", StudentController, :exit_room
    get "/room/:room_id", StudentController, :join_room
  end

  scope "/project", PlatformWeb do
    pipe_through [:browser, :migrate_old_user]
    get "/preview/*slug", StudentController, :show_project

    pipe_through :require_authenticated_user
    get "/init/*slug", StudentController, :init_project
    live "/:project_id", StudentMissionLive, :index
  end

  scope "/teach", PlatformWeb do
    pipe_through [:browser, :migrate_old_user]
    get "/", TeacherController, :index

    pipe_through :require_authenticated_user
    get "/new-room", TeacherController, :new
    post "/new-room", TeacherController, :create
    live "/project/:project_id", TeacherMissionLive, :project
    live "/room/:room_id", TeacherMissionLive, :index
    live "/room/:room_id/project/:project_id", TeacherMissionLive, :index
  end

  scope "/coach", PlatformWeb do
    pipe_through [:browser, :admin_auth]
    get "/", CoachController, :index
  end

  scope "/result", PlatformWeb do
    pipe_through [:browser, :result_layout]
    live "/:type/:project_id/", ResultFrameLive, :index
    live "/:type/:project_template_id/:lesson_hash", ResultFrameLive, :index
  end

  scope "/admin", PlatformWeb do
    pipe_through [:browser, :admin_auth, :require_authenticated_user]
    live_dashboard "/sysmon", metrics: PlatformWeb.Telemetry
    resources "/users", UserController
    resources "/rooms", RoomController
    resources "/projects", ProjectController
    resources "/project_templates", ProjectTemplateController
    resources "/courses", CourseController
    resources "/", AdminController
  end

  scope "/sdb" do
    pipe_through :api
    get "/:db_name", Platform.Router.StudentDBController, :get
    get "/:db_name/since/:since_id", Platform.Router.StudentDBController, :get
    post "/:db_name", Platform.Router.StudentDBController, :push
    get "/:db_name/drop", Platform.Router.StudentDBController, :drop
  end

  if Application.get_env(:platform, Platform.Mailer)[:adapter] == Swoosh.Adapters.Local do
    scope "/mailbox" do
      pipe_through [:browser]
      forward "/", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", PlatformWeb do
    pipe_through [:browser, :redirect_if_user_is_registered]
    get "/users/name", UserCreationController, :new
    post "/users/name", UserCreationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create

    pipe_through :require_authenticated_user
    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    put "/users/register", UserRegistrationController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", PlatformWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings", UserSettingsController, :update
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email
  end

  scope "/", PlatformWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :edit
    post "/users/confirm/:token", UserConfirmationController, :update
  end
end
