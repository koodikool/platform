defmodule PlatformWeb.ChatComponent do
  alias Phoenix.PubSub
  alias Platform.ProjectStateAgent
  alias Platform.Models
  alias Platform.Models.ChatMessage
  alias Platform.Accounts
  use Phoenix.LiveView
  import Destructure

  @doc """
  A chat box. Can only be used inside a LiveView at the moment.
  """

  def mount(:not_mounted_at_router, session, socket) do
    s(%{from, parent_id, parent_type}) = session
    params = d(%{from, parent_id, parent_type})
    PubSub.subscribe(Platform.PubSub, "chat_messages:#{parent_type}:#{parent_id}")
    messages = Models.list_chat_messages_with_users(params) |> Enum.reverse()
    {:ok, assign(socket, messages: messages, params: d(%{from, parent_id, parent_type}))}
  end

  def handle_event("send-message", %{"value" => message}, socket) do
    params = socket.assigns.params

    {:ok, message_db} =
      params
      |> Map.put(:message, message)
      |> Models.create_chat_message()

    PubSub.broadcast(
      Platform.PubSub,
      "chat_messages:#{params.parent_type}:#{params.parent_id}",
      {:new_chat_message, message_db}
    )

    with %{parent_type: "project", parent_id: project_id, from: from} <- params,
         %{created_by: created_by} <- Models.get_project(project_id),
         true <- created_by == from do
      ProjectStateAgent.update(project_id, :chat_unread, true)
    end

    {:noreply, socket}
  end

  def handle_event("mark_read", _values, socket) do
    with %{parent_type: "project", parent_id: project_id, from: from} <- socket.assigns.params,
         %{created_by: created_by} <- Models.get_project(project_id),
         false <- created_by == from do
      ProjectStateAgent.update(project_id, :chat_unread, false)
    end

    {:noreply, socket}
  end

  def handle_info({:new_chat_message, %ChatMessage{} = new_message}, socket) do
    user = Accounts.get_user!(new_message.from)
    messages = socket.assigns.messages ++ [{new_message, user}]
    {:noreply, assign(socket, messages: messages)}
  end
end
