<nav class="flex bg-white justify-between">
    <div></div>
    <img class="h-32" src="/images/tinkr-logo-01.svg" style="color: #3c3c3c" alt="Tinkr logo">
    <div></div>
</nav>
<section class="w-screen bg-contain bg-right-bottom bg-no-repeat" style="background-image: url(https://tinkr.tech/static/llama.png); background-color: #90cfde;">
    <div class="max-w-3xl m-auto p-20 text-center">
        <p class="text-4xl text-white">Learning to program is like<br>learning a language.<br><b>If you can read this text,<br>we can teach you how to code.</b></p>
        <button class="rounded-full bg-white mt-6 py-4 w-52"><b>FIND OUT HOW</b></button>
    </div>
</section>
<section class="w-screen" style="background-color: #e9e9e9;">
    <div class="max-w-3xl m-auto p-20 text-center">
        <p class="text-2xl"><b>TINKR is an entry level web developer course,</b><br>where you carry out your first 5 real projects:<br>3 websites, a chat app and a game.<br>No long, boring theory –<br>we throw you right into the water!</p>
        <button class="rounded-full bg-white mt-6 py-4 w-52"><b>BEGINKR</b></button>
    </div>
</section>
<section class="w-screen" style="background-color: #0000ff;">
    <div class="max-w-3xl m-auto p-20 text-center">
        <p class="text-2xl text-white"><b>Let’s get to know each other,<br>by making your own personal website together!</b><br>Tinkr is a very practical way to learn<br>programming. No long, boring theory –<br>we throw you right into the water!</p>
        <button class="rounded-full bg-white mt-6 py-4 w-52"><b>HOP IN</b></button>
    </div>
</section>
<section class="w-screen relative" style="background-color: #e9e9e9;">
    <div class="max-w-5xl m-auto p-28 text-center grid grid-cols-2 gap-16 text-xl">
        <p><b>Learn from professionals</b><br>At Tinkr you get support from real specialists in our field. There are no stupid questions, only stupid not to ask.</p>
        <p><b>The start of your portfolio</b><br>Our curriculum is very practical. You come out of here with a mini-portfolio that includes real websites and apps that open the doors of schools and bootcamps for you.</p>
        <p><b>Learn at your own speed</b><br>We don’t care if you want to jump in the deep end to get the knowledge quickly and at once, or paddle around on weekends. Learn as fast as your pace of life allows.</p>
        <p><b>Nobody will judge you</b><br>It doesn’t matter if you are still in school or if you’re looking to change careers in your 40’s. We have seen security guards, taxi drivers, and shopkeepers who have become developers. Everyone is welcome.</p>
    </div>
    <div class="half-circle"></div>
</section>
<section class="w-screen" style="background-color: #181857;">
    <div class="max-w-3xl m-auto p-20 text-center">
        <p class="text-2xl text-white"><b>Programming is an exciting and well-paid profession</b><br>Imagine that your new skills will be used to create websites and apps that will make people’s lives around the world easier, more comfortable, or more entertaining. What do you have to lose?</p>
        <button class="rounded-full bg-white mt-6 py-4 w-52"><b>TRY IT FREE</b></button>
    </div>
</section>
<section class="w-screen relative" style="background-color: #cfa1eb;">
    <div class="max-w-3xl m-auto p-20 pb-32 text-center">
        <p class="text-2xl text-white"><b>Could even I become a web developer?</b><br>Code is like a language. If you can talk, then you canprogram. We have seen security guards, taxi drivers andshopkeepers who have become web developers.</p>
        <button class="rounded-full bg-white mt-6 py-4 w-52"><b>TRY IT FREE</b></button>
    </div>
    <div class="half-circle"></div>
</section>
<section>
    <div class="bg-white p-6 pb-20">
        <button class="button--janus button-tympanus"><span>Try it free</span></button>
        <button class="button--surtur button-tympanus">
                <svg class="textcircle" viewBox="0 0 500 500">
                    <title>- Learn by tinkering - Tinkr</title>
                    <defs><path id="textcircle" d="M250,400 a150,150 0 0,1 0,-300a150,150 0 0,1 0,300Z"
                    /></defs>
                    <text><textPath xlink:href="#textcircle" aria-label="- Learn by tinkering - Tinkr" textLength="900">- Learn by tinkering - Tinkr</textPath></text>
                </svg>
                <svg aria-hidden="true" class="eye" width="70" height="70" viewBox="0 0 70 70" xmlns="http://www.w3.org/2000/svg">
                    <path class="eye__outer" d="M10.5 35.308c5.227-7.98 14.248-13.252 24.5-13.252s19.273 5.271 24.5 13.252c-5.227 7.98-14.248 13.253-24.5 13.253s-19.273-5.272-24.5-13.253z"/>
                    <path class="eye__lashes-up" d="M35 8.802v8.836M49.537 11.383l-3.31 8.192M20.522 11.684l3.31 8.192" />
                    <path class="eye__lashes-down" d="M35 61.818v-8.836 8.836zM49.537 59.237l-3.31-8.193 3.31 8.193zM20.522 58.936l3.31-8.193-3.31 8.193z" />
                    <circle class="eye__iris" cx="35" cy="35.31" r="5.221" />
                    <circle class="eye__inner" cx="35" cy="35.31" r="10.041" />
                </svg>
            </button>
    </div>
</section>

<style>
@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap');

body {
    color: #3c3c3c;
    font-family: 'Montserrat', sans-serif;
}
section > div > p.text-4xl {
    line-height: 3rem;
}
section > div > p.text-2xl {

    line-height: 2.9rem;
}
.half-circle {
    height: 5rem;
    width: 10rem;
    background-color: white;
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
    border-top-left-radius: 10rem;
    border-top-right-radius: 10rem;
    z-index: 1;
}

/* BUTTONS */
.button-tympanus {
	pointer-events: auto;
	cursor: pointer;
	background: #e7e7e7;
	border: none;
	padding: 1.5rem 3rem;
	margin: 0;
	font-family: inherit;
	font-size: inherit;
	position: relative;
	display: inline-block;
}
.button-tympanus:hover {
    background: none;
}
.button-tympanus::before,
.button-tympanus::after {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
.button--janus {
	font-family: freight-display-pro, serif;
	font-weight: 900;
	width: 175px;
	height: 120px;
	color: #fff;
	background: none;
    z-index: 1;
}

.button--janus::before {
	content: '';
	background: #e6e6e6;
	-webkit-clip-path: path("M154.5,88.5 C131,113.5 62.5,110 30,89.5 C-2.5,69 -3.5,42 4.5,25.5 C12.5,9 33.5,-6 85,3.5 C136.5,13 178,63.5 154.5,88.5 Z");
	clip-path: path("M154.5,88.5 C131,113.5 62.5,110 30,89.5 C-2.5,69 -3.5,42 4.5,25.5 C12.5,9 33.5,-6 85,3.5 C136.5,13 178,63.5 154.5,88.5 Z");
	transition: clip-path 0.5s cubic-bezier(0.585, 2.5, 0.645, 0.55), -webkit-clip-path 0.5s cubic-bezier(0.585, 2.5, 0.645, 0.55), background 0.5s ease;
}

.button--janus:hover::before {
	background: #000;
	-webkit-clip-path: path("M143,77 C117,96 74,100.5 45.5,91.5 C17,82.5 -10.5,57 5.5,31.5 C21.5,6 79,-5.5 130.5,4 C182,13.5 169,58 143,77 Z");
	clip-path: path("M143,77 C117,96 74,100.5 45.5,91.5 C17,82.5 -10.5,57 5.5,31.5 C21.5,6 79,-5.5 130.5,4 C182,13.5 169,58 143,77 Z");
}

.button--janus::after {
	content: '';
	height: 86%;
	width: 97%;
	top: 5%;
	border-radius: 58% 42% 55% 45% / 56% 45% 55% 44%;
	border: 1px solid #000;
	transform: rotate(-20deg);
	z-index: -1;
	transition: transform 0.5s cubic-bezier(0.585, 2.5, 0.645, 0.55);
}

.button--janus:hover::after {
	transform: translate3d(0,-5px,0);
}

.button--janus span {
	display: block;
	transition: transform 0.3s ease;
	mix-blend-mode: difference;
}

.button--janus:hover span {
	transform: translate3d(0,-10px,0);
}

.button--surtur {
	padding: 0;
	background: none;
	-webkit-clip-path: circle(40% at 50% 50%);
	clip-path: circle(40% at 50% 50%);
}

.button--surtur:focus-visible {
	background: #443ffc;
}

.textcircle {
	position: relative;
	display: block;
	width: 200px;
}

.textcircle text {
	font-size: 32px;
	text-transform: uppercase;
	fill: #000;
}

.textcircle textPath {
	letter-spacing: 17px; /* Firefox needs this */
}

.button--surtur:hover .textcircle {
	animation: rotateIt 7s linear infinite;
}

.eye {
	position: absolute;
	z-index: 2;
	width: 60px;
	height: 60px;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
}

.eye__outer,
.eye__inner,
.eye__lashes-up,
.eye__lashes-down {
	stroke: #000;
	fill: none;
	stroke-width: 1.5px;
}

.eye__lashes-down {
	opacity: 0;
}

.button--surtur:hover .eye__lashes-up,
.button--surtur:hover .eye__inner,
.button--surtur:hover .eye__iris {
	animation: blinkHide 2s step-end infinite;
}

.button--surtur:hover .eye__lashes-down {
	animation: blinkShow 2s step-end infinite;
}

@keyframes blinkHide {
	0% {
		opacity: 0;
	}
	10% {
		opacity: 1;
	}
}

@keyframes blinkShow {
	0% {
		opacity: 1;
	}
	10% {
		opacity: 0;
	}
}
@keyframes rotateIt {
  to {
	transform: rotate(-360deg);
  }
}
</style>
