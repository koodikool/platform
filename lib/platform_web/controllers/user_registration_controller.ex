defmodule PlatformWeb.UserRegistrationController do
  use PlatformWeb, :controller

  alias Platform.Accounts
  alias Platform.Accounts.User
  alias PlatformWeb.UserAuth

  def new(conn, params) do
    conn = UserAuth.maybe_store_return_to_params(conn, params)

    changeset = Accounts.change_user_registration(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    user = conn.assigns.current_user

    case Accounts.register_user(user, user_params) do
      {:ok, user} ->
        {:ok, _} =
          Accounts.deliver_user_confirmation_instructions(
            user,
            &Routes.user_confirmation_url(conn, :edit, &1)
          )

        conn
        |> UserAuth.log_in_user(user)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end
