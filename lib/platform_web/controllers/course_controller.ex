defmodule PlatformWeb.CourseController do
  use PlatformWeb, :controller

  alias Platform.Models
  alias Platform.Models.Course

  def index(conn, _params) do
    courses = Models.list_courses()
    render(conn, "index.html", courses: courses)
  end

  def new(conn, _params) do
    changeset = Models.change_course(%Course{})
    chunked_templates = divide_templates(nil)
    render(conn, "new.html", changeset: changeset, project_templates: chunked_templates)
  end

  def create(conn, %{"course" => course_params}) do
    course_params = Map.update(course_params, "project_templates", [], &String.split(&1, ","))

    case Models.create_course(course_params) do
      {:ok, course} ->
        conn
        |> put_flash(:info, "Course created successfully.")
        |> redirect(to: Routes.course_path(conn, :show, course))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    course = Models.get_course!(id)
    render(conn, "show.html", course: course)
  end

  def edit(conn, %{"id" => id}) do
    course = Models.get_course!(id)
    changeset = Models.change_course(course)

    chunked_templates = divide_templates(course.project_templates)

    render(conn, "edit.html",
      course: course,
      changeset: changeset,
      project_templates: chunked_templates
    )
  end

  def update(conn, %{"id" => id, "course" => course_params}) do
    course_params =
      Map.update(course_params, "project_templates", [], &String.split(&1, ",", trim: true))

    course = Models.get_course!(id)

    case Models.update_course(course, course_params) do
      {:ok, course} ->
        conn
        |> put_flash(:info, "Course updated successfully.")
        |> redirect(to: Routes.course_path(conn, :show, course))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html",
          course: course,
          changeset: changeset,
          project_templates: course_params |> Map.get("project_templates") |> divide_templates
        )
    end
  end

  def delete(conn, %{"id" => id}) do
    course = Models.get_course!(id)
    {:ok, _course} = Models.delete_course(course)

    conn
    |> put_flash(:info, "Course deleted successfully.")
    |> redirect(to: Routes.course_path(conn, :index))
  end

  defp divide_templates(nil) do
    project_templates = Models.list_project_templates()
    {[], project_templates}
  end

  defp divide_templates(included_template_ids) do
    project_templates = Models.list_project_templates()

    included_templates =
      Enum.map(included_template_ids, fn inc_id ->
        Enum.find(project_templates, &(&1.id == inc_id))
      end)

    excluded_templates =
      Enum.filter(project_templates, &(!Enum.member?(included_template_ids, &1.id)))

    {included_templates, excluded_templates}
  end
end
