defmodule PlatformWeb.UserCreationController do
  use PlatformWeb, :controller

  alias Platform.Accounts
  alias Platform.Models
  alias PlatformWeb.UserAuth

  def new(conn, _params) do
    render(conn, "new.html", error_message: nil)
  end

  def create(conn, %{"user" => user_params}) do
    return_url = get_session(conn, :user_return_to) || ""

    room_id =
      return_url
      |> String.starts_with?("/learn/room/")
      |> case do
        false ->
          nil

        true ->
          String.trim_leading(return_url, "/learn/room/")
          |> String.split("?")
          |> List.first()
      end

    collision =
      case room_id do
        nil -> nil
        room_id -> Map.get(user_params, "name") |> Models.get_room_collision(room_id)
      end

    case collision do
      nil ->
        case Accounts.create_user(user_params) do
          {:ok, user} ->
            UserAuth.log_in_user(conn, user)

          {:error, %Ecto.Changeset{}} ->
            render(conn, "new.html", error_message: "Invalid name")
        end

      _collision ->
        conn
        |> render("new.html", error_message: "Name is taken, add your surname.")
    end
  end
end
