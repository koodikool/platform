defmodule PlatformWeb.CoachController do
  alias Platform.Models
  alias Platform.Accounts
  alias Platform.ProjectStateAgent
  alias Platform.ProjectState

  use PlatformWeb, :controller

  def index(conn, _params) do
    rooms =
      for room <- Models.list_rooms() do
        user = Accounts.get_user!(room.created_by)

        Map.merge(room, %{
          username: user.name
        })
      end

    projects =
      for snapshot <- Models.list_project_snapshots_coach(has_updates: true) do
        project = Map.get(snapshot.project, "project_id") |> Models.get_project!()
        snapshot_state = ProjectState.from_project(project, snapshot)
        agent_state = ProjectStateAgent.get_no_init(project.id) || %{}
        user = Accounts.get_user!(snapshot_state.created_by)
        room = (project.room_id && Models.get_room!(project.room_id)) || %{}
        project_template = Models.get_project_template!(snapshot_state.project_template_id)

        project
        |> Map.merge(snapshot_state)
        |> Map.merge(agent_state)
        |> Map.merge(%{
          username: user.name,
          room_id: project.room_id,
          room_name: Map.get(room, :name),
          title: project_template.title,
          last_updated_at: snapshot.inserted_at
        })
      end

    render(conn, "index.html", rooms: rooms, projects: projects)
  end
end
