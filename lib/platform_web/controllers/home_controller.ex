defmodule PlatformWeb.HomeController do
  use PlatformWeb, :controller

  def available_paths do
    ["/", "/contact", "platform", "curriculum", "projects", "pricing", "about"]
  end

  def static_page(conn, _params) do
    page = (conn.path_info |> List.last()) <> ".html"
    render(conn, page, %{})
  end

  def static_page_abtest(conn, _params) do
    path = Enum.at(conn.path_info, 0, "index")
    {conn, page} = ab_test(conn, path)
    render(conn, page, %{})
  end

  defp ab_test(conn, requested_path) do
    memory = get_session(conn, :ab_test_memory) || %{}
    page = Map.get(memory, requested_path) || random_variation(requested_path)
    new_memory = Map.put(memory, requested_path, page)
    conn = put_session(conn, :ab_test_memory, new_memory)
    {conn, page}
  end

  defp random_variation(page) do
    PlatformWeb.HomeView.__templates__()
    |> elem(2)
    |> Enum.filter(&String.starts_with?(&1, page))
    |> Enum.random()
  end
end
