defmodule Platform.Router.StudentDBController do
  use PlatformWeb, :controller
  alias Platform.StudentDBAgent

  @limits Application.get_env(:platform, Platform.StudentDBAgent)[:ratelimits]

  def get(conn, %{"db_name" => db_name, "since_id" => since_id}) do
    results = StudentDBAgent.get_all_since(db_name, since_id)
    json(conn, results)
  end

  def get(conn, %{"db_name" => db_name}) do
    {conn, sdb_key} = get_set_key(conn)

    case Hammer.check_rate("sdb_action:#{sdb_key}", @limits.period, @limits.count) do
      {:allow, _count} ->
        results = StudentDBAgent.get_all(db_name)
        json(conn, results)

      {:deny, _limit} ->
        send_resp(conn, :too_many_requests, "")
    end
  end

  def push(conn, %{"id" => _id}) do
    send_resp(conn, 400, "Field 'id' is reserved by the database.")
  end

  def push(%Plug.Conn{body_params: body_params} = conn, _params) when not is_map(body_params) do
    send_resp(conn, 400, "Only {map} type objects allowed.")
  end

  def push(
        %Plug.Conn{
          body_params: body_params,
          path_params: %{"db_name" => db_name}
        } = conn,
        _params
      )
      when is_map(body_params) do
    {conn, sdb_key} = get_set_key(conn)

    case Hammer.check_rate("sdb_action:#{sdb_key}", @limits.period, @limits.count) do
      {:allow, _count} ->
        StudentDBAgent.push(db_name, body_params)
        send_resp(conn, :created, "")

      {:deny, _limit} ->
        send_resp(conn, :too_many_requests, "")
    end
  end

  def drop(conn, %{"db_name" => db_name}) do
    StudentDBAgent.drop(db_name)
    send_resp(conn, :accepted, "Database deleted: #{db_name}")
  end

  def get_set_key(conn) do
    case get_session(conn, :sdb_key) do
      nil ->
        key = Ecto.UUID.generate()
        conn = put_session(conn, :sdb_key, key)
        {conn, key}

      key ->
        {conn, key}
    end
  end
end
