defmodule PlatformWeb.TeacherController do
  use PlatformWeb, :controller

  alias Platform.Models
  alias Platform.Models.Room

  def index(conn, _params) do
    rooms =
      with %{id: user_id} <- conn.assigns.current_user do
        Models.list_rooms(user_id)
        |> Enum.map(fn room ->
          projects = Models.list_projects(%{room_id: room.id})

          template_title =
            room.force_project_template_id
            |> case do
              nil -> nil
              template_id -> Models.get_project_template!(template_id) |> Map.get(:title)
            end

          room
          |> Map.merge(%{
            project_count: length(projects),
            forced_template_title: template_title
          })
        end)
      else
        _ -> []
      end

    render(conn, "index.html", rooms: rooms)
  end

  def new(conn, _params) do
    user_id = conn.assigns.current_user.id

    project_templates =
      Models.list_project_templates(%{user_id: user_id})
      |> Enum.map(fn p -> {p.slug, p.id} end)

    changeset = Models.change_room(%Room{})

    render(conn, "new.html",
      changeset: changeset,
      action: Routes.teacher_path(conn, :create),
      project_templates: project_templates
    )
  end

  def create(conn, %{
        "room" => %{
          "name" => name,
          "force_project_template_id" => template_id,
          "free_flow" => free_flow
        }
      }) do
    room = %{
      name: name,
      created_by: conn.assigns.current_user.id,
      force_project_template_id: template_id,
      free_flow: free_flow
    }

    case Models.create_room(room) do
      {:ok, room} ->
        conn |> redirect(to: Routes.teacher_mission_path(conn, :index, room))

      {:error, %Ecto.Changeset{} = changeset} ->
        user_id = conn.assigns.current_user.id

        projects =
          Models.list_project_templates(%{user_id: user_id})
          |> Enum.map(fn p -> {p.slug, p.id} end)

        render(conn, "new.html",
          changeset: changeset,
          action: Routes.teacher_path(conn, :create),
          projects: projects
        )
    end
  end
end
