defmodule PlatformWeb.DaysOfCodeController do
  use PlatformWeb, :controller
  alias Platform.Models

  def index(conn, _params) do
    data_active = Models.list_days_with_tweets()
    data_quit = Models.list_days_with_tweets(:quit)

    stats = Enum.map(1..100, &{&1, Map.get(data_active, &1, []), Map.get(data_quit, &1, [])})

    max =
      stats
      |> Enum.map(fn {_i, active, quit} -> length(active) + length(quit) end)
      |> Enum.max()

    render(conn, "index.html", stats: stats, max: max)
  end
end
