defmodule PlatformWeb.ProjectTemplateController do
  use PlatformWeb, :controller

  alias Platform.Models
  alias Platform.Models.ProjectTemplate

  def index(conn, _params) do
    project_templates = Models.list_project_templates_all()
    render(conn, "index.html", project_templates: project_templates)
  end

  def new(conn, _params) do
    changeset = Models.change_project_template(%ProjectTemplate{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"project_template" => project_template_params}) do
    project_template_params =
      Map.put(project_template_params, "created_by", conn.assigns.current_user.id)

    case Models.create_project_template(project_template_params) do
      {:ok, project_template} ->
        conn
        |> put_flash(:info, "Course project created successfully.")
        |> redirect(to: Routes.project_template_path(conn, :show, project_template))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    project_template = Models.get_project_template!(id)
    render(conn, "show.html", project_template: project_template)
  end

  def edit(conn, %{"id" => id}) do
    project_template = Models.get_project_template!(id)
    changeset = Models.change_project_template(project_template)
    render(conn, "edit.html", project_template: project_template, changeset: changeset)
  end

  def update(conn, %{"id" => id, "project_template" => project_template_params}) do
    project_template = Models.get_project_template!(id)

    case Models.update_project_template(project_template, project_template_params) do
      {:ok, project_template} ->
        conn
        |> put_flash(:info, "Course project updated successfully.")
        |> redirect(to: Routes.project_template_path(conn, :show, project_template))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", project_template: project_template, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    project_template = Models.get_project_template!(id)
    {:ok, _project_template} = Models.delete_project_template(project_template)

    conn
    |> put_flash(:info, "Course project deleted successfully.")
    |> redirect(to: Routes.project_template_path(conn, :index))
  end
end
