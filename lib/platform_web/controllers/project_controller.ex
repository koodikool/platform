defmodule PlatformWeb.ProjectController do
  use PlatformWeb, :controller

  alias Platform.Models
  alias Platform.Models.Project

  def index(conn, _params) do
    projects = Models.list_projects()
    render(conn, "index.html", projects: projects)
  end

  def new(conn, _params) do
    changeset = Models.change_project(%Project{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"project" => project_params}) do
    case Models.create_project(project_params) do
      {:ok, project} ->
        conn
        |> put_flash(:info, "Student project created successfully.")
        |> redirect(to: Routes.project_path(conn, :show, project))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    project = Models.get_project!(id)
    render(conn, "show.html", project: project)
  end

  def edit(conn, %{"id" => id}) do
    project = Models.get_project!(id)
    changeset = Models.change_project(project)
    render(conn, "edit.html", project: project, changeset: changeset)
  end

  def update(conn, %{"id" => id, "project" => project_params}) do
    project = Models.get_project!(id)

    case Models.update_project(project, project_params) do
      {:ok, project} ->
        conn
        |> put_flash(:info, "Student project updated successfully.")
        |> redirect(to: Routes.project_path(conn, :show, project))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", project: project, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    Models.delete_all_project_snapshots(%{project_id: id})

    project = Models.get_project!(id)
    {:ok, _project} = Models.delete_project(project)

    conn
    |> put_flash(:info, "Student project deleted successfully.")
    |> redirect(to: Routes.project_path(conn, :index))
  end
end
