defmodule PlatformWeb.StudentController do
  use PlatformWeb, :controller
  import Destructure
  alias Platform.Models
  alias Platform.Models.Project
  alias Platform.Models.Course
  alias Platform.Models.ProjectTemplate
  alias Platform.Accounts

  @doc ~S"""
  Show all available projects. There are some rules:
  1. Show all public templates, or templates that user has created.
  1. Group templates by courses.
  1. One template can be present in multiple courses.
  1. Courses must maintain the order of projects defined in DB.
  1. Any templates and projects that are not part of any course are displayed separately.
  """
  def list_projects(conn, _params) do
    user_id =
      with %{current_user: %{id: user_id}} <- conn.assigns do
        user_id
      else
        _ -> nil
      end

    current_room_id =
      with %{current_user: %{current_room_id: current_room_id}} <- conn.assigns do
        current_room_id
      else
        _ -> nil
      end

    user_projects = Models.list_projects(%{user_id: user_id})
    courses = Models.list_courses()

    course_template_ids =
      courses
      |> Enum.map(& &1.project_templates)
      |> List.flatten()
      |> MapSet.new()
      |> MapSet.to_list()

    no_course_templates = Models.list_project_templates_not_in(course_template_ids, user_id)

    no_template_projects =
      no_course_templates
      |> Enum.map(& &1.id)
      |> Enum.concat(course_template_ids)
      |> then(fn template_ids ->
        Enum.filter(user_projects, &(!Enum.member?(template_ids, &1.project_template_id)))
      end)

    leftovers_course = %Course{
      project_templates: no_course_templates ++ no_template_projects,
      title: gettext("Ungrouped projects")
    }

    course_map =
      courses
      |> Enum.concat([leftovers_course])
      |> Enum.map(fn d(%{title, project_templates}) ->
        projects =
          project_templates
          |> ProjectTemplate.get_all_in_order!()
          |> Project.replace_templates(user_projects)

        {title, projects}
      end)

    room_name =
      case Models.get_room!(current_room_id) do
        nil -> nil
        room -> Map.get(room, :name)
      end

    render(conn, "list_projects.html", room: room_name, course_map: course_map)
  end

  def show_project(conn, %{"slug" => slug}) do
    project_template = Models.get_project_template!(%{slug: Enum.join(slug, "/")})
    render(conn, "show_project.html", project_template: project_template)
  end

  def init_project(conn, %{"slug" => slug}) do
    %{current_room_id: current_room_id, id: user_id} = conn.assigns.current_user

    free_flow =
      case current_room_id do
        nil -> true
        str when is_bitstring(str) -> Models.get_room!(current_room_id) |> Map.get(:free_flow)
      end

    project_template = Models.get_project_template!(%{slug: Enum.join(slug, "/")})

    {:ok, project} =
      Models.create_project(%{
        created_by: user_id,
        project_template_id: project_template.id,
        room_id: current_room_id,
        free_flow: free_flow || false
      })

    redirect(conn, to: Routes.student_mission_path(conn, :index, project.id))
  end

  def join_room(conn, s(%{room_id})) do
    {:ok, _user} =
      conn.assigns.current_user
      |> Accounts.update_user_room(room_id)

    Models.get_room!(room_id)
    |> case do
      %{force_project_template_id: nil} ->
        redirect(conn, to: Routes.student_path(conn, :list_projects))

      %{force_project_template_id: project_template_id} ->
        Models.get_project_template!(project_template_id)
        |> Map.get(:slug)
        |> then(&show_project(conn, %{"slug" => String.split(&1, "/")}))
    end
  end

  def exit_room(conn, _params) do
    {:ok, _user} =
      conn.assigns.current_user
      |> Accounts.update_user_room(nil)

    redirect(conn, to: Routes.student_path(conn, :list_projects))
  end

  def mdn_search_index(conn, _params) do
    case HTTPoison.get("https://developer.mozilla.org/en-US/search-index.json") do
      {:ok, response} ->
        conn
        |> put_resp_content_type(
          response.headers
          |> List.keyfind("Content-Type", 0)
          |> elem(0)
        )
        |> send_resp(response.status_code, response.body)

      {:error, _error} ->
        conn |> put_status(:bad_gateway)
    end
  end
end
