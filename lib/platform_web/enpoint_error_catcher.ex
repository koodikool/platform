defmodule PlatformWeb.EndpointErrorCatcher do
  @moduledoc """
  Since Phoenix doesn't capture all errors we've got to try-catch them ourselves. I have no idea how Phoenix is meant to be working in this regard, but I just had an invisible error in prod.
  Module is ripped from https://github.com/getsentry/sentry-elixir/blob/02a2cc31c7444af88d079f7c32a789b9b1b4e6f1/lib/sentry/plug_capture.ex
  #### Usage
  In a Phoenix application, it is important to use this module before
  the Phoenix endpoint itself. It should be added to your endpoint.ex:
      defmodule MyApp.Endpoint
        use EndpointErrorCatcher
        use Phoenix.Endpoint, otp_app: :my_app
        # ...
      end
  In a Plug application, it can be added below your router:
      defmodule MyApp.PlugRouter do
        use Plug.Router
        use EndpointErrorCatcher
        # ...
      end
  """
  defmacro __using__(_opts) do
    quote do
      @before_compile PlatformWeb.EndpointErrorCatcher
    end
  end

  defmacro __before_compile__(_) do
    quote do
      defoverridable call: 2

      def call(conn, opts) do
        try do
          super(conn, opts)
        rescue
          e in Plug.Conn.WrapperError ->
            Logger.error("EndpointErrorCatcher WrapperError")

            stack =
              e.stack
              |> List.first()
              |> elem(3)
              |> case do
                [] -> "no_stack"
                [file: file, line: line] -> "#{file}:#{line}"
                other -> inspect(other)
              end

            message =
              with %{reason: reason} <- e,
                   %{message: message} <- reason do
                message
              end

            Logger.error("EndpointWrapperError at #{stack}: #{inspect(message)}")
            Plug.Conn.WrapperError.reraise(e)

          e in Phoenix.Router.NoRouteError ->
            Logger.error("404 - #{e.message}")
            :erlang.raise(:error, e, __STACKTRACE__)

          e ->
            Logger.error("EndpointErrorCatcher")

            stack =
              __STACKTRACE__
              |> List.first()
              |> elem(3)
              |> then(fn [file: file, line: line] -> "#{file}:#{line}" end)

            message =
              with %{reason: reason} <- e,
                   %{message: message} <- reason do
                message
              end

            Logger.error("EndpointUnknownError at #{stack}: #{inspect(message)}")

            :erlang.raise(:error, e, __STACKTRACE__)
        catch
          kind, reason ->
            message = "Uncaught #{kind} - #{inspect(reason)}"
            stack = __STACKTRACE__
            Logger.error(message, stacktrace: stack)
            :erlang.raise(kind, reason, stack)
        end
      end
    end
  end
end
