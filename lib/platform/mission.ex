defmodule Platform.Mission do
  alias Platform.Models
  alias Platform.ProjectLessons
  alias Platform.ProjectState
  alias Platform.ProjectStateAgent
  alias Platform.Mission
  alias Platform.Models.Project
  alias Platform.Models.ProjectTemplate
  alias Platform.Models.ProjectSnapshot
  alias Platform.Accounts

  import Destructure
  use TypedStruct

  typedstruct enforce: true do
    field :username, String.t()
    field :project_id, String.t()
    field :free_flow, boolean()
    field :completed, boolean()
    field :ready_next, boolean()
    field :asking_help, boolean()
    field :has_updates, boolean()
    field :project_template_id, String.t()
    field :hint_timer, integer()
    field :hints_total, %ProjectState.Hints{}
    field :hints, %ProjectState.Hints{}
    field :code, %ProjectState.Code{}
    field :lesson, %ProjectLessons.Lesson{}
  end

  @spec get(nil | bitstring | Project | ProjectState) :: nil | %Mission{}
  def get(nil) do
    nil
  end

  def get(project_id) when is_bitstring(project_id) do
    project_id
    |> Models.get_project()
    |> get()
  end

  def get(%Project{} = project) do
    project.id
    |> ProjectStateAgent.get()
    |> get
  end

  def get(%ProjectState{} = project_state) do
    with user when not is_nil(user) <- Accounts.get_user!(project_state.created_by),
         d(%{project_template_id, lesson_hash}) = project_state,
         lesson <- ProjectLessons.get_lesson(project_template_id, lesson_hash) do
      hints_total =
        for {ext, file} <- lesson.files,
            hints = Map.get(file, :hints),
            into: %{} do
          {ext, length(hints)}
        end

      %{
        username: user.name,
        project_id: project_state.project_id,
        free_flow: project_state.free_flow,
        completed: project_state.completed,
        ready_next: project_state.ready_next,
        asking_help: project_state.asking_help,
        has_updates: project_state.has_updates,
        project_template_id: project_state.project_template_id,
        code: project_state.code,
        hints: project_state.hints,
        hints_total: hints_total,
        hint_timer: project_state.hint_timer,
        lesson: Map.take(lesson, [:title, :files, :hash, :progressStr])
      }
    else
      _ -> nil
    end
  end

  @spec next_lesson(String.t(), String.t()) :: any
  def next_lesson(project_id, current_lesson_hash) do
    project = get(project_id)

    ProjectLessons.get_next_lesson(project.project_template_id, current_lesson_hash)
    |> case do
      nil ->
        ProjectStateAgent.update(project_id, :completed, true)

      %{hash: hash} ->
        ProjectStateAgent.update(project_id, :lesson_hash, hash)
        ProjectStateAgent.update(project_id, :hints, %ProjectState.Hints{})
    end

    ProjectStateAgent.update(project_id, :ready_next, false)
    ProjectSnapshot.save_snapshot(project_id)
  end

  @spec previous_lesson(String.t(), String.t()) :: any
  def previous_lesson(project_id, current_lesson_hash) do
    project = get(project_id)

    ProjectLessons.get_next_lesson(project.project_template_id, current_lesson_hash, -1)
    |> case do
      nil ->
        nil

      %{hash: hash} ->
        ProjectStateAgent.update(project_id, :lesson_hash, hash)
        ProjectStateAgent.update(project_id, :hints, %ProjectState.Hints{})
    end

    ProjectStateAgent.update(project_id, :ready_next, false)
    ProjectSnapshot.save_snapshot(project_id)
  end

  @spec hint_timer(String.t(), integer) :: nil
  def hint_timer(project_id, remaining \\ 3)

  def hint_timer(_project_id, -1) do
    nil
  end

  def hint_timer(project_id, remaining) do
    ProjectStateAgent.update(project_id, :hint_timer, remaining)
    Process.sleep(60000)
    hint_timer(project_id, remaining - 1)
  end

  def get_project_card(%ProjectTemplate{} = template) do
    template
  end

  def get_project_card(%Project{} = project) do
    %{name: room_name} =
      case project.room_id do
        nil -> %{name: nil}
        room_id -> Models.get_room!(room_id)
      end

    %{completed: completed, lesson_hash: lesson_hash} = ProjectStateAgent.get(project.id)

    %{progressStr: progressStr} =
      ProjectLessons.get_lesson(project.project_template_id, lesson_hash)

    project.project_template_id
    |> Models.get_project_template!()
    |> Map.take([:slug, :title, :description])
    |> Map.merge(project)
    |> Map.put(:room_name, room_name)
    |> Map.put(:progressStr, progressStr)
    |> Map.put(:completed, completed)
  end
end
