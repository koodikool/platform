defmodule Platform.Hints do
  @spec make_hints(binary | maybe_improper_list, atom) :: list

  def make_hints(diff, ext) when is_bitstring(diff) do
    diff
    |> clean_diff()
    |> make_hints(ext)
  end

  def make_hints(diff, :html) do
    diff = clean_html(diff)

    [
      diff |> get_nested_html() |> Enum.sort() |> filter_minus(),
      diff |> filter_minus(),
      diff
    ]
    |> Enum.reject(&Enum.empty?/1)
    |> Enum.uniq_by(fn hint ->
      for row <- hint do
        row
        |> String.trim()
        |> String.trim_leading("+")
        |> String.trim()
      end
    end)
  end

  def make_hints(diff, _ext) do
    [
      diff |> Enum.sort() |> filter_minus(),
      diff |> filter_minus(),
      diff
    ]
    |> Enum.reject(&Enum.empty?/1)
    |> Enum.uniq()
  end

  defp clean_diff(diff) do
    diff
    |> String.split("\n")
    |> Enum.filter(fn row ->
      row = String.trim_leading(row)

      !String.starts_with?(row, ["+++", "---"]) and
        String.starts_with?(row, ["+", "-"]) and
        !Enum.member?(["+", "-"], row)
    end)
  end

  defp get_nested_html(diff) when is_list(diff) do
    diff
    |> Enum.filter(fn row ->
      row = String.trim_leading(row)
      String.starts_with?(row, "+")
    end)
    |> Enum.join("\n")
    |> get_nested_html()
    |> Enum.map(fn row -> "+" <> row end)
  end

  defp get_nested_html(str) when is_bitstring(str) do
    match = Regex.scan(~r/<(.*?).*?>(.*?)<\/\1>/is, str)
    deep = Enum.map(match, fn m -> get_nested_html(Enum.fetch!(m, 2)) end)

    match_self_closing = Regex.scan(~r/<(?:(?!<).)*?\/>/is, str)

    match_unclosed = Regex.scan(~r/<[img|hr|br]{2,}(?:(?!<).)*?>/is, str)

    match
    |> Enum.map(fn
      [_m, "script", ""] -> ""
      [_m, "script", "\n"] -> ""
      [_m, "script", "\r\n"] -> ""
      [_m, "link", ""] -> ""
      [m, _, ""] -> m
      [m, _tag, content] -> String.replace(m, content, "Change me")
    end)
    |> Enum.concat(deep)
    |> Enum.concat(match_self_closing)
    |> Enum.concat(match_unclosed)
    |> List.flatten()
    |> Enum.uniq()
    |> Enum.reject(&(&1 == ""))
  end

  defp clean_html(diff) do
    exclude = ["script.js", "style.css"]
    Enum.reject(diff, &String.contains?(&1, exclude))
  end

  defp filter_minus(diff) do
    Enum.reject(diff, fn row ->
      row
      |> String.trim_leading()
      |> String.starts_with?("-")
    end)
  end

  # def make_hints(diff, ext) do
  #   [
  #     make_hint(:plus_only, diff) |> hint_finish(ext),
  #     make_hint(:with_minus, diff) |> hint_finish(ext),
  #     diff |> hint_finish(ext)
  #   ]
  #   |> Enum.reject(&Enum.empty?/1)
  #   |> Enum.uniq()
  # end

  # @spec make_hint(atom, [String.t()]) :: [String.t()]
  # defp make_hint(:plus_only, diff) do
  #   Enum.filter(diff, fn row ->
  #     String.starts_with?(row, "+")
  #   end)
  # end

  # defp make_hint(:with_minus, diff) do
  #   Enum.filter(diff, fn row ->
  #     String.starts_with?(row, "+") or String.starts_with?(row, "-")
  #   end)
  # end

  # @spec hint_finish([String.t()] | String.t(), atom) :: [String.t()]
  # defp hint_finish(hint, _ext) when is_list(hint) do
  #   hint
  #   |> Enum.uniq()
  #   |> Enum.filter(fn line -> line != "" end)
  #   |> Enum.sort()
  # end
end
