defmodule Platform.Models do
  @moduledoc """
  The Models context.
  """
  import Ecto.Query, warn: false
  alias Platform.Repo
  alias Platform.Models.Project
  alias Platform.Models.Room
  alias Platform.Accounts.User

  @doc """
  Returns the list of rooms.

  ## Examples

      iex> list_rooms()
      [%Room{}, ...]

  """
  def list_rooms do
    Repo.all(Room)
  end

  def list_rooms(user_id) do
    from(r in Room, where: r.created_by == ^user_id)
    |> Repo.all()
  end

  @doc """
  Gets a single room.

  Raises `Ecto.NoResultsError` if the Room does not exist.

  ## Examples

      iex> get_room!(123)
      %Room{}

      iex> get_room!(456)
      ** (Ecto.NoResultsError)

  """
  def get_room!(nil), do: nil
  def get_room!(id), do: Repo.get!(Room, id)

  @doc """
  Check if username has a collision in this room.
  """
  def get_room_collision(user_name, room_id) do
    from(u in User,
      join: p in Project,
      on: u.id == p.created_by,
      join: r in Room,
      on: r.id == p.room_id,
      where:
        u.name == ^user_name and
          r.id == ^room_id,
      select: [u.name, r.id]
    )
    |> Repo.one()
  end

  @doc """
  Creates a room.

  ## Examples

      iex> create_room(%{field: value})
      {:ok, %Room{}}

      iex> create_room(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_room(attrs \\ %{}) do
    %Room{}
    |> Room.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a room.

  ## Examples

      iex> update_room(room, %{field: new_value})
      {:ok, %Room{}}

      iex> update_room(room, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_room(%Room{} = room, attrs) do
    room
    |> Room.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a room.

  ## Examples

      iex> delete_room(room)
      {:ok, %Room{}}

      iex> delete_room(room)
      {:error, %Ecto.Changeset{}}

  """
  def delete_room(%Room{} = room) do
    Repo.delete(room)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking room changes.

  ## Examples

      iex> change_room(room)
      %Ecto.Changeset{data: %Room{}}

  """
  def change_room(%Room{} = room, attrs \\ %{}) do
    Room.changeset(room, attrs)
  end

  alias Platform.Models.Project

  @doc """
  Returns the list of projects.

  ## Examples

      iex> list_projects()
      [%Project{}, ...]

  """
  def list_projects do
    Repo.all(Project)
  end

  def list_projects(%{room_id: room_id}) do
    from(p in Project,
      where: p.room_id == ^room_id
    )
    |> Repo.all()
  end

  def list_projects(%{user_id: nil}) do
    []
  end

  def list_projects(%{user_id: user_id}) do
    from(p in Project,
      where: p.created_by == ^user_id
    )
    |> Repo.all()
  end

  @doc """
  Gets a single project.

  Raises `Ecto.NoResultsError` if the Student project does not exist.

  ## Examples

      iex> get_project!(123)
      %Project{}

      iex> get_project!(456)
      ** (Ecto.NoResultsError)

  """
  def get_project(%{created_by: created_by}), do: Repo.get(Project, created_by: created_by)
  def get_project(id), do: Repo.get(Project, id)
  def get_project!(%{created_by: created_by}), do: Repo.get!(Project, created_by: created_by)
  def get_project!(id), do: Repo.get!(Project, id)

  @doc """
  Creates a project.

  ## Examples

      iex> create_project(%{field: value})
      {:ok, %Project{}}

      iex> create_project(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_project(attrs \\ %{}) do
    %Project{}
    |> Project.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a project.

  ## Examples

      iex> update_project(project, %{field: new_value})
      {:ok, %Project{}}

      iex> update_project(project, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_project(%Project{} = project, attrs) do
    project
    |> Project.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a project.

  ## Examples

      iex> delete_project(project)
      {:ok, %Project{}}

      iex> delete_project(project)
      {:error, %Ecto.Changeset{}}

  """
  def delete_project(%Project{} = project) do
    Repo.delete(project)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking project changes.

  ## Examples

      iex> change_project(project)
      %Ecto.Changeset{data: %Project{}}

  """
  def change_project(%Project{} = project, attrs \\ %{}) do
    Project.changeset(project, attrs)
  end

  alias Platform.Models.ProjectTemplate

  @doc """
  Returns the list of project_templates.

  ## Examples

      iex> list_project_templates()
      [%ProjectTemplate{}, ...]

  """
  def list_project_templates_all do
    Repo.all(ProjectTemplate)
  end

  def list_project_templates do
    from(pt in ProjectTemplate,
      where: pt.public == true
    )
    |> Repo.all()
  end

  def list_project_templates(%{user_id: nil}) do
    list_project_templates()
  end

  def list_project_templates(%{user_id: user_id}) do
    from(pt in ProjectTemplate,
      where: pt.public == true or pt.created_by == ^user_id
    )
    |> Repo.all()
  end

  def list_project_templates_not_in(template_ids, nil) do
    from(pt in ProjectTemplate,
      where: pt.id not in ^template_ids and pt.public == true
    )
    |> Repo.all()
  end

  def list_project_templates_not_in(template_ids, user_id) do
    from(pt in ProjectTemplate,
      where: pt.id not in ^template_ids and (pt.public == true or pt.created_by == ^user_id)
    )
    |> Repo.all()
  end

  @doc """
  Gets a single project_template.

  Raises `Ecto.NoResultsError` if the Course project does not exist.

  ## Examples

      iex> get_project_template!(123)
      %ProjectTemplate{}

      iex> get_project_template!(456)
      ** (Ecto.NoResultsError)

  """
  def get_project_template!(%{slug: slug}) do
    from(pt in ProjectTemplate,
      where: pt.slug == ^slug,
      limit: 1
    )
    |> Repo.one!()
  end

  def get_project_template!(id), do: Repo.get!(ProjectTemplate, id)

  @doc """
  Creates a project_template.

  ## Examples

      iex> create_project_template(%{field: value})
      {:ok, %ProjectTemplate{}}

      iex> create_project_template(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_project_template(attrs \\ %{}) do
    %ProjectTemplate{}
    |> ProjectTemplate.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a project_template.

  ## Examples

      iex> update_project_template(project_template, %{field: new_value})
      {:ok, %ProjectTemplate{}}

      iex> update_project_template(project_template, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_project_template(%ProjectTemplate{} = project_template, attrs) do
    project_template
    |> ProjectTemplate.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a project_template.

  ## Examples

      iex> delete_project_template(project_template)
      {:ok, %ProjectTemplate{}}

      iex> delete_project_template(project_template)
      {:error, %Ecto.Changeset{}}

  """
  def delete_project_template(%ProjectTemplate{} = project_template) do
    Repo.delete(project_template)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking project_template changes.

  ## Examples

      iex> change_project_template(project_template)
      %Ecto.Changeset{data: %ProjectTemplate{}}

  """
  def change_project_template(%ProjectTemplate{} = project_template, attrs \\ %{}) do
    ProjectTemplate.changeset(project_template, attrs)
  end

  alias Platform.Models.ProjectSnapshot

  @doc """
  Returns the list of project_snapshots.

  ## Examples

      iex> list_project_snapshots()
      [%ProjectSnapshot{}, ...]

  """
  def list_project_snapshots do
    Repo.all(ProjectSnapshot)
  end

  def list_project_snapshots(where) do
    from(ps in ProjectSnapshot,
      where: ^where
    )
    |> Repo.all()
  end

  def list_project_snapshots_coach(where) do
    query_last =
      from(ps in ProjectSnapshot,
        select: %ProjectSnapshot{
          project_id: fragment("DISTINCT ON (?) ?", ps.project_id, ps.project_id),
          id: ps.id,
          inserted_at: ps.inserted_at,
          updated_at: ps.updated_at,
          version: ps.version,
          has_updates: ps.has_updates,
          project: ps.project
        },
        order_by: [ps.project_id, desc: ps.inserted_at]
      )

    active =
      from(ps in subquery(query_last),
        where: ^where,
        order_by: [desc: ps.inserted_at]
      )
      |> Repo.all()

    last =
      from(ps in subquery(query_last),
        limit: 30,
        order_by: [desc: ps.inserted_at]
      )
      |> Repo.all()

    (active ++ last)
    |> Enum.uniq()
  end

  def get_last_snapshot_from_lesson_hash(lesson_hash) do
    from(ps in ProjectSnapshot,
      where: fragment("? ->> ? = ?", ps.project, "lesson_hash", ^lesson_hash),
      limit: 1
    )
    |> Repo.one()
  end

  @doc """
  Gets a single project_snapshot.

  Raises `Ecto.NoResultsError` if the Project snapshot does not exist.

  ## Examples

      iex> get_project_snapshot!(123)
      %ProjectSnapshot{}

      iex> get_project_snapshot!(456)
      ** (Ecto.NoResultsError)

  """
  def get_project_snapshot!(id), do: Repo.get!(ProjectSnapshot, id)
  def get_project_snapshot(id), do: Repo.get(ProjectSnapshot, id)

  def get_last_project_snapshot(id),
    do: ProjectSnapshot |> where(project_id: ^id) |> last(:inserted_at) |> Repo.one()

  @doc """
  Creates a project_snapshot.

  ## Examples

      iex> create_project_snapshot(%{field: value})
      {:ok, %ProjectSnapshot{}}

      iex> create_project_snapshot(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_project_snapshot(attrs \\ %{}) do
    %ProjectSnapshot{}
    |> ProjectSnapshot.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a project_snapshot.

  ## Examples

      iex> update_project_snapshot(project_snapshot, %{field: new_value})
      {:ok, %ProjectSnapshot{}}

      iex> update_project_snapshot(project_snapshot, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_project_snapshot(%ProjectSnapshot{} = project_snapshot, attrs) do
    project_snapshot
    |> ProjectSnapshot.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a project_snapshot.

  ## Examples

      iex> delete_project_snapshot(project_snapshot)
      {:ok, %ProjectSnapshot{}}

      iex> delete_project_snapshot(project_snapshot)
      {:error, %Ecto.Changeset{}}

  """
  def delete_project_snapshot(%ProjectSnapshot{} = project_snapshot) do
    Repo.delete(project_snapshot)
  end

  def delete_all_project_snapshots(%{project_id: project_id}) do
    from(ps in ProjectSnapshot,
      where: ps.project_id == ^project_id
    )
    |> Repo.delete_all()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking project_snapshot changes.

  ## Examples

      iex> change_project_snapshot(project_snapshot)
      %Ecto.Changeset{data: %ProjectSnapshot{}}

  """
  def change_project_snapshot(%ProjectSnapshot{} = project_snapshot, attrs \\ %{}) do
    ProjectSnapshot.changeset(project_snapshot, attrs)
  end

  alias Platform.Models.ChatMessage

  @doc """
  Returns the list of chat_messages.

  ## Examples

      iex> list_chat_messages()
      [%ChatMessage{}, ...]

  """
  def list_chat_messages_with_users(%{parent_type: parent_type, parent_id: parent_id}) do
    from(cm in ChatMessage,
      where: cm.parent_type == ^parent_type and cm.parent_id == ^parent_id,
      join: u in User,
      on: u.id == cm.from,
      select: {cm, u},
      order_by: [desc: cm.inserted_at],
      limit: 50
    )
    |> Repo.all()
  end

  def list_chat_messages do
    Repo.all(ChatMessage)
  end

  @doc """
  Gets a single chat_message.

  Raises `Ecto.NoResultsError` if the Chat message does not exist.

  ## Examples

      iex> get_chat_message!(123)
      %ChatMessage{}

      iex> get_chat_message!(456)
      ** (Ecto.NoResultsError)

  """
  def get_chat_message!(id), do: Repo.get!(ChatMessage, id)

  @doc """
  Creates a chat_message.

  ## Examples

      iex> create_chat_message(%{field: value})
      {:ok, %ChatMessage{}}

      iex> create_chat_message(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_chat_message(attrs \\ %{}) do
    %ChatMessage{}
    |> ChatMessage.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a chat_message.

  ## Examples

      iex> update_chat_message(chat_message, %{field: new_value})
      {:ok, %ChatMessage{}}

      iex> update_chat_message(chat_message, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_chat_message(%ChatMessage{} = chat_message, attrs) do
    chat_message
    |> ChatMessage.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a chat_message.

  ## Examples

      iex> delete_chat_message(chat_message)
      {:ok, %ChatMessage{}}

      iex> delete_chat_message(chat_message)
      {:error, %Ecto.Changeset{}}

  """
  def delete_chat_message(%ChatMessage{} = chat_message) do
    Repo.delete(chat_message)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking chat_message changes.

  ## Examples

      iex> change_chat_message(chat_message)
      %Ecto.Changeset{data: %ChatMessage{}}

  """
  def change_chat_message(%ChatMessage{} = chat_message, attrs \\ %{}) do
    ChatMessage.changeset(chat_message, attrs)
  end

  alias Platform.Models.DayOfCode

  @doc """
  Returns the list of days_of_code.

  ## Examples

      iex> list_days_of_code()
      [%DayOfCode{}, ...]

  """
  def list_days_of_code do
    Repo.all(DayOfCode)
  end

  def list_days_with_tweets(period \\ :active) do
    # SELECT day, json_agg(status_id)
    # FROM (SELECT DISTINCT ON (user_id) day, status_id
    # FROM days_of_code
    # ORDER BY user_id, inserted_at DESC) AS distinct_tweets
    # GROUP BY day;
    two_days_ago = Timex.now() |> Timex.shift(days: -2)
    week_ago = Timex.now() |> Timex.shift(days: -7)

    distinct_tweets =
      from(doc in DayOfCode,
        select: %{
          user_id: fragment("DISTINCT ON (?) ?", doc.user_id, doc.user_id),
          day: doc.day,
          status_id: doc.status_id
        },
        order_by: [desc: doc.user_id, desc: doc.inserted_at]
      )

    distinct_tweets =
      case period do
        :active ->
          from(doc in distinct_tweets, where: doc.inserted_at > ^two_days_ago)

        :quit ->
          from(doc in distinct_tweets,
            where: doc.inserted_at < ^two_days_ago and doc.inserted_at > ^week_ago
          )
      end

    from(doc in subquery(distinct_tweets),
      select: {doc.day, fragment("json_agg(?)", doc.status_id)},
      group_by: doc.day
    )
    |> Repo.all()
    |> Map.new()
  end

  @doc """
  Gets a single day_of_code.

  Raises `Ecto.NoResultsError` if the Day of code does not exist.

  ## Examples

      iex> get_day_of_code!(123)
      %DayOfCode{}

      iex> get_day_of_code!(456)
      ** (Ecto.NoResultsError)

  """
  def get_day_of_code!(id), do: Repo.get!(DayOfCode, id)

  @doc """
  Creates a day_of_code.

  ## Examples

      iex> create_day_of_code(%{field: value})
      {:ok, %DayOfCode{}}

      iex> create_day_of_code(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_day_of_code(attrs \\ %{}) do
    %DayOfCode{}
    |> DayOfCode.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a day_of_code.

  ## Examples

      iex> update_day_of_code(day_of_code, %{field: new_value})
      {:ok, %DayOfCode{}}

      iex> update_day_of_code(day_of_code, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_day_of_code(%DayOfCode{} = day_of_code, attrs) do
    day_of_code
    |> DayOfCode.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a day_of_code.

  ## Examples

      iex> delete_day_of_code(day_of_code)
      {:ok, %DayOfCode{}}

      iex> delete_day_of_code(day_of_code)
      {:error, %Ecto.Changeset{}}

  """
  def delete_day_of_code(%DayOfCode{} = day_of_code) do
    Repo.delete(day_of_code)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking day_of_code changes.

  ## Examples

      iex> change_day_of_code(day_of_code)
      %Ecto.Changeset{data: %DayOfCode{}}

  """
  def change_day_of_code(%DayOfCode{} = day_of_code, attrs \\ %{}) do
    DayOfCode.changeset(day_of_code, attrs)
  end

  alias Platform.Models.Course

  @doc """
  Returns the list of courses.

  ## Examples

      iex> list_courses()
      [%Course{}, ...]

  """
  def list_courses do
    Repo.all(Course)
  end

  @doc """
  Gets a single course.

  Raises `Ecto.NoResultsError` if the Course does not exist.

  ## Examples

      iex> get_course!(123)
      %Course{}

      iex> get_course!(456)
      ** (Ecto.NoResultsError)

  """
  def get_course!(id), do: Repo.get!(Course, id)

  @doc """
  Creates a course.

  ## Examples

      iex> create_course(%{field: value})
      {:ok, %Course{}}

      iex> create_course(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_course(attrs \\ %{}) do
    %Course{}
    |> Course.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a course.

  ## Examples

      iex> update_course(course, %{field: new_value})
      {:ok, %Course{}}

      iex> update_course(course, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_course(%Course{} = course, attrs) do
    course
    |> Course.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a course.

  ## Examples

      iex> delete_course(course)
      {:ok, %Course{}}

      iex> delete_course(course)
      {:error, %Ecto.Changeset{}}

  """
  def delete_course(%Course{} = course) do
    Repo.delete(course)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking course changes.

  ## Examples

      iex> change_course(course)
      %Ecto.Changeset{data: %Course{}}

  """
  def change_course(%Course{} = course, attrs \\ %{}) do
    Course.changeset(course, attrs)
  end
end
