defmodule Platform.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      Platform.Repo,
      PlatformWeb.Telemetry,
      {Phoenix.PubSub, name: Platform.PubSub},
      PlatformWeb.Endpoint,
      Platform.ProjectStateAgent,
      {Task.Supervisor, name: Platform.TaskSupervisor},
      Platform.Backup,
      {ConCache, [name: :project_template_view, ttl_check_interval: false]}
    ]

    children =
      case Application.get_env(:extwitter, :oauth)[:enabled] do
        true -> children ++ [Platform.DaysOfCode]
        _ -> children
      end

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Platform.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PlatformWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
