defmodule Platform.RoomOverview do
  alias Platform.RoomOverview
  alias Platform.Models
  alias Platform.ProjectStateAgent
  alias Platform.ProjectLessons
  alias Platform.Accounts
  import Destructure
  use TypedStruct

  typedstruct enforce: true do
    field :project_id, String.t()
    field :project_name, String.t()
    field :username, String.t()
    field :room_id, String.t()
    field :progressStr, String.t()
    field :ready_next, boolean()
    field :asking_help, boolean()
    field :chat_unread, boolean()
    field :has_updates, boolean()
    field :completed, boolean()
  end

  @spec get(bitstring) :: [%RoomOverview{}]
  def get(room_id) when is_bitstring(room_id) do
    for p <- Models.list_projects(d(%{room_id})) do
      project = ProjectStateAgent.get(p.id)
      d(%{project_template_id, lesson_hash}) = project
      lesson = ProjectLessons.get_lesson(project_template_id, lesson_hash)
      user = Accounts.get_user!(project.created_by)
      project_template = Models.get_project_template!(project.project_template_id)

      %RoomOverview{
        project_id: p.id,
        project_name: project_template.title,
        ready_next: project.ready_next,
        asking_help: project.asking_help,
        chat_unread: project.chat_unread,
        has_updates: project.has_updates,
        completed: project.completed,
        room_id: p.room_id,
        progressStr: lesson.progressStr,
        username: user.name
      }
    end
  end
end
