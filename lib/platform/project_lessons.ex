defmodule Platform.ProjectLessons do
  alias Platform.Models
  alias Platform.Hints
  alias Platform.GitUtils
  alias Platform.Models

  use TypedStruct

  typedstruct module: LessonFile, enforce: true do
    field :filename, String.t()
    field :content_end, String.t()
    field :hints, [String.t()]
  end

  typedstruct module: Lesson, enforce: true do
    field :hash, String.t()
    field :title, String.t()
    field :progressStr, String.t()

    field :files, %{
      html: %LessonFile{},
      css: %LessonFile{},
      js: %LessonFile{},
      backgroundjs: String.t()
    }
  end

  typedstruct enforce: true do
    @typedoc "Lessons that are served to the student."

    field :template_id, String.t()
    field :lesson_count, non_neg_integer()
    field :lessons, [%Lesson{}]
  end

  @spec get(String.t()) :: [ProjectLessons.t()]
  def get(project_template_id) do
    ConCache.get_or_store(:project_template_view, project_template_id, fn ->
      get(project_template_id, true)
    end)
  end

  @spec get(String.t(), true) :: [ProjectLessons.t()]
  def get(project_template_id, true) do
    template = Models.get_project_template!(project_template_id)

    {_repo_url, repo} = GitUtils.download_template(template.git_repo)
    parse_lessons(repo)
  end

  @spec parse_lessons(Git.Repository.t()) :: [Lesson.t()]
  defp parse_lessons(%Git.Repository{path: _repo_path} = repo) do
    {:ok, log_string} = Git.log(repo, ~w(--format=format:"%H"))

    commits =
      log_string
      |> String.split("\n")

    lesson_count = length(commits)

    commits
    |> Enum.map(fn str -> String.trim(str, "\"") end)
    |> Enum.reverse()
    |> Enum.with_index()
    |> Enum.map(fn {hash, i} -> parse_lesson(repo, hash, {i, lesson_count}) end)
  end

  @spec parse_lesson(Git.Repository.t(), String.t(), {integer, integer}) :: Lesson.t()
  defp parse_lesson(%Git.Repository{} = repo, hash, {done, total}) do
    [title, filenames] =
      with {:ok, title_raw} = Git.show(repo, ~w(-s --format=%s #{hash})),
           {:ok, files_raw} = Git.ls_tree(repo, ~w(--name-only -r #{hash})),
           title = String.trim_trailing(title_raw, "\n"),
           filenames = files_raw |> String.trim_trailing() |> String.split("\n") do
        [title, filenames]
      end

    files =
      for filename <- filenames do
        case filename do
          "background.js" ->
            {:backgroundjs, %{content_end: Git.show!(repo, ~w(#{hash}:#{filename}))}}

          _ ->
            filename
            |> String.split(".")
            |> List.last()
            |> case do
              "html" -> {:html, parse_file(repo, hash, filename, :html)}
              "css" -> {:css, parse_file(repo, hash, filename)}
              "js" -> {:js, parse_file(repo, hash, filename)}
            end
        end
      end
      |> Map.new()

    %Lesson{
      hash: hash,
      title: title,
      progressStr: "#{done + 1} / #{total}",
      files: files
    }
  end

  @spec parse_file(Git.Repository.t(), String.t(), String.t(), atom) :: LessonFile.t()
  defp parse_file(repo, hash, filename, ext \\ :any) do
    diff = Git.show!(repo, ~w(#{hash} -- #{filename}))

    hints = Hints.make_hints(diff, ext)

    %LessonFile{
      filename: filename,
      content_end: get_file_content(repo, hash, filename, ext),
      hints: hints
    }
  end

  @spec get_file_content(Git.Repository.t(), String.t(), String.t(), atom) :: String.t()
  defp get_file_content(repo, hash, filename, :html) do
    Git.show!(repo, ~w(#{hash}:#{filename}))
    |> String.replace(~r/<script.*?script>/s, "", global: true)
    |> String.replace(~r/<link.*?>/s, "", global: true)
  end

  defp get_file_content(repo, hash, filename, _ext) do
    Git.show!(repo, ~w(#{hash}:#{filename}))
  end

  @spec get_lesson(String.t(), String.t()) :: Lesson.t()
  def get_lesson(project_template_id, lesson_hash) do
    project_template_id
    |> Platform.ProjectLessons.get()
    |> Enum.find(fn l -> l.hash == lesson_hash end)
    |> case do
      nil -> get_lesson_last_ancestor(project_template_id, lesson_hash)
      lesson -> lesson
    end
  end

  @doc """
  If project git repo changes let's find the last common commit between
  new repo and user completed lessons and rewind lessons back there. Actually
  versioning the repo is better, but the effort/reward doesn't seem reasonable
  with basically 0 users.
  """
  def get_lesson_last_ancestor(project_template_id, lesson_hash) do
    new_commits =
      project_template_id
      |> Platform.ProjectLessons.get()
      |> Enum.map(& &1.hash)

    %{project_id: project_id} = Models.get_last_snapshot_from_lesson_hash(lesson_hash)

    user_commits =
      Models.list_project_snapshots(project_id: project_id)
      |> Enum.map(& &1.project["lesson_hash"])
      |> Enum.uniq()

    ancestor_commit =
      user_commits
      |> Enum.take_while(&(&1 != lesson_hash))
      |> Enum.reverse()
      |> Enum.find_value(fn user_commit ->
        Enum.member?(new_commits, user_commit) && user_commit
      end)

    get_lesson(project_template_id, ancestor_commit)
  end

  @spec get_next_lesson(String.t(), String.t(), integer) :: Lesson.t() | nil
  def get_next_lesson(project_template_id, current_lesson_hash, skip \\ 1) do
    lessons = Platform.ProjectLessons.get(project_template_id)
    index = Enum.find_index(lessons, fn l -> l.hash == current_lesson_hash end)
    new_index = max(index + skip, 0)
    Enum.at(lessons, new_index)
  end
end
