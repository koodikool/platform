defmodule Platform.GitUtils do
  require Logger

  @data_dir_default Application.get_env(:platform, Platform.GitUtils)[:data_dir_default]

  def download_template(repo_url) do
    {:ok, path} = prepare_dir(repo_url)

    case Git.clone([repo_url, path]) do
      {:ok, repo} ->
        {repo_url, repo}

      {:error, %Git.Error{code: 128}} ->
        repo = Git.new(path)
        branch = Git.branch!(repo) |> String.trim_leading("* ") |> String.trim_trailing("\n")
        Git.fetch!(repo)
        Git.reset!(repo, ["--hard", "origin/#{branch}"])
        {repo_url, repo}

      {:error, reason} ->
        Logger.warn(reason, label: "reason")
    end
  end

  defp prepare_dir(repo_url, data_dir \\ @data_dir_default) do
    full_path =
      repo_url
      |> String.trim_trailing(".git")
      |> String.trim_leading("https://")
      |> (&Path.join(data_dir, &1)).()

    File.mkdir_p(full_path)
    |> case do
      :ok ->
        {:ok, full_path}

      {:error, :enoent} ->
        Logger.error("Directory doesn't exist: #{data_dir}")
        :error

      {:error, :eaccess} ->
        Logger.error("Directory permissions not set: #{data_dir}")
        :error

      {:error, :enospc} ->
        Logger.error("Disk full: #{full_path}")
        :error
    end
  end
end
