defmodule Platform.DaysOfCode do
  alias Platform.Models
  alias Platform.Models.DayOfCode
  require Logger

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent,
      shutdown: 500
    }
  end

  def start_link([]) do
    Logger.info("Starting Twitter stream")

    pid =
      spawn_link(fn ->
        stream =
          ExTwitter.stream_filter([track: "100daysofcode", receive_messages: true], :infinity)

        for message <- stream do
          case message do
            tweet = %ExTwitter.Model.Tweet{} ->
              handle_tweet(tweet)

            deleted_tweet = %ExTwitter.Model.DeletedTweet{} ->
              Logger.info("deleted tweet = #{deleted_tweet.status[:id]}")

            limit = %ExTwitter.Model.Limit{} ->
              Logger.info("limit = #{limit.track}")

            stall_warning = %ExTwitter.Model.StallWarning{} ->
              Logger.info("stall warning = #{stall_warning.code}")

            _ ->
              Logger.warn(message)
          end
        end
      end)

    {:ok, pid}
  end

  def handle_tweet(%ExTwitter.Model.Tweet{retweeted_status: nil} = tweet) do
    tweet.text
    |> DayOfCode.parse_day()
    |> case do
      nil ->
        nil

      day ->
        Models.create_day_of_code(%{user_id: tweet.user.id_str, status_id: tweet.id_str, day: day})
    end
  end

  def handle_tweet(%ExTwitter.Model.Tweet{}) do
    nil
  end
end
