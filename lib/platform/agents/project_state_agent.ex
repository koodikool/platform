defmodule Platform.ProjectStateAgent do
  alias Phoenix.PubSub
  alias Platform.Backup
  alias Platform.Models
  alias Platform.ProjectState

  use Agent

  @spec start_link([]) :: {:error, any} | {:ok, pid}
  def start_link([]) do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  @spec get(String.t()) :: ProjectState.t() | nil
  def get(project_id) when is_bitstring(project_id) do
    Agent.get(__MODULE__, &Map.get(&1, project_id))
    |> case do
      nil -> init_project_state(project_id)
      project -> project
    end
  end

  @doc """
    Exactly the same as get(), but wont do heavy lifting like fetch snapshot
  """
  @spec get_no_init(String.t()) :: ProjectState.t() | nil
  def get_no_init(project_id) when is_bitstring(project_id) do
    Agent.get(__MODULE__, &Map.get(&1, project_id))
    |> case do
      nil -> nil
      project -> project
    end
  end

  @spec put(String.t(), ProjectState.t()) :: any
  def put(project_id, %ProjectState{} = new_project_state) when is_bitstring(project_id) do
    Agent.update(__MODULE__, fn state ->
      Map.put(state, project_id, new_project_state)
    end)

    PubSub.broadcast(
      Platform.PubSub,
      "project_update:#{project_id}",
      {:project_update, new_project_state}
    )

    Backup.project_updated(project_id)

    case new_project_state do
      %{room_id: nil} ->
        nil

      %{room_id: room_id} ->
        PubSub.broadcast(
          Platform.PubSub,
          "room_update:#{room_id}",
          {:project_update, new_project_state}
        )
    end
  end

  @spec update(String.t(), atom, any) :: any
  def update(project_id, key, val) when is_atom(key) do
    case get(project_id) do
      nil ->
        {:error, :project_no_exist}

      project ->
        project
        |> Map.put(key, val)
        |> set_has_updates(key)
        |> then(&put(project_id, &1))
    end
  end

  defp set_has_updates(project, key) do
    case key do
      :code -> Map.put(project, :has_updates, true)
      _ -> project
    end
  end

  @spec init_project_state(String.t()) :: ProjectState | nil
  defp init_project_state(project_id) when is_bitstring(project_id) do
    project = Models.get_project(project_id)
    snapshot = Models.get_last_project_snapshot(project_id)

    case {project, snapshot} do
      {nil, _} ->
        nil

      {project, nil} ->
        project
        |> ProjectState.from_project(nil)
        |> tap(&put(project_id, &1))

      {project, snapshot} ->
        project
        |> ProjectState.from_project(snapshot)
        |> tap(&put(project_id, &1))
    end
  end
end
