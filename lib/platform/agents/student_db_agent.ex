defmodule Platform.StudentDBAgent do
  use Agent

  def start(db_name) do
    Agent.start(fn -> [] end, name: {:global, db_name})
    |> case do
      {:ok, pid} -> {:ok, pid}
      {:error, {:already_started, pid}} -> {:ok, pid}
    end
  end

  def get_all(db_name) do
    {:ok, pid} = start(db_name)
    Agent.get(pid, & &1)
  end

  def get_all_since(db_name, since_id) do
    {:ok, pid} = start(db_name)

    Agent.get(pid, fn state ->
      state
      |> Enum.reverse()
      |> Enum.take_while(fn %{id: id} -> id != since_id end)
      |> Enum.reverse()
    end)
  end

  def push(db_name, value) when is_map(value) do
    {:ok, pid} = start(db_name)
    value = Map.put(value, :id, Platform.Utils.random_string(16))
    Agent.update(pid, fn state -> limit_length(state ++ [value]) end)
  end

  def push(_db_name, value) when not is_map(value) do
    {:error, :not_map}
  end

  def drop(db_name) do
    {:ok, pid} = start(db_name)
    Agent.stop(pid)
  end

  defp limit_length(state) do
    state
    |> Enum.reverse()
    |> Enum.take(limit())
    |> Enum.reverse()
  end

  defp limit() do
    Application.get_env(:platform, Platform.StudentDBAgent)[:student_db_size_limit]
  end
end
