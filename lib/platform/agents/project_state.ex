defmodule Platform.ProjectState do
  alias Platform.ProjectState
  alias Platform.ProjectLessons
  alias Platform.Models
  alias Platform.Models.Project
  alias Platform.Models.ProjectSnapshot
  use TypedStruct

  typedstruct module: Code, enforce: true do
    field :html, String.t(), default: ""
    field :css, String.t(), default: ""
    field :js, String.t(), default: ""
  end

  typedstruct module: Hints, enforce: true do
    field :html, non_neg_integer(), default: 0
    field :css, non_neg_integer(), default: 0
    field :js, non_neg_integer(), default: 0
  end

  typedstruct enforce: true do
    field :project_id, String.t()
    field :project_template_id, String.t()
    field :created_by, String.t()
    field :lesson_hash, String.t()
    field :code, %Code{}, default: %Code{}
    field :room_id, String.t() | nil, default: nil
    field :free_flow, boolean(), default: true
    field :ready_next, boolean(), default: false
    field :asking_help, boolean(), default: false
    field :chat_unread, boolean(), default: false
    field :completed, boolean(), default: false
    field :has_updates, boolean(), default: false
    field :hint_timer, integer(), default: 0
    field :hints, %Hints{}, default: %Hints{}
  end

  @spec from_project(Project.t(), ProjectSnapshot.t() | nil) :: ProjectState.t()
  def from_project(%Project{} = project, nil) do
    lessons = ProjectLessons.get(project.project_template_id)
    lesson = List.first(lessons)

    ProjectState
    |> struct(Map.from_struct(project))
    |> struct(%{lesson_hash: lesson.hash, project_id: project.id})
  end

  def from_project(nil, %ProjectSnapshot{} = snapshot) do
    project = Map.get(snapshot.project, "project_id") |> Models.get_project!()
    from_project(project, snapshot)
  end

  def from_project(%Project{} = project, %ProjectSnapshot{} = snapshot) do
    %{project: snapshot} = ProjectSnapshot.migrate_snapshot(snapshot)

    ProjectState
    |> struct(Map.from_struct(project))
    |> struct(%{
      code: %ProjectState.Code{
        html: get_in(snapshot, ["code", "html"]),
        css: get_in(snapshot, ["code", "css"]),
        js: get_in(snapshot, ["code", "js"])
      },
      asking_help: get_in(snapshot, ["asking_help"]),
      chat_unread: get_in(snapshot, ["chat_unread"]),
      completed: get_in(snapshot, ["completed"]),
      created_by: get_in(snapshot, ["created_by"]),
      free_flow: get_in(snapshot, ["free_flow"]),
      lesson_hash: get_in(snapshot, ["lesson_hash"]),
      project_id: get_in(snapshot, ["project_id"]),
      project_template_id: get_in(snapshot, ["project_template_id"]),
      ready_next: get_in(snapshot, ["ready_next"]),
      room_id: project.room_id,
      has_updates: get_in(snapshot, ["has_updates"]),
      hints: %ProjectState.Hints{
        html: get_in(snapshot, ["hints", "html"]),
        css: get_in(snapshot, ["hints", "css"]),
        js: get_in(snapshot, ["hints", "js"])
      }
    })
  end
end
