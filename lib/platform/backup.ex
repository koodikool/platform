defmodule Platform.Backup do
  alias Platform.Models.ProjectSnapshot
  use Agent

  @spec start_link([]) :: {:error, any} | {:ok, pid}
  def start_link([]) do
    Agent.start_link(fn -> MapSet.new() end, name: __MODULE__)

    Task.Supervisor.start_child(
      Platform.TaskSupervisor,
      fn ->
        backup_loop()
      end,
      restart: :permanent
    )
  end

  def project_updated(project_id) when is_bitstring(project_id) do
    Agent.update(__MODULE__, fn state ->
      MapSet.put(state, project_id)
    end)
  end

  defp backup_loop() do
    Process.sleep(60000)

    Agent.get_and_update(__MODULE__, fn state -> {state, MapSet.new()} end)
    |> Enum.map(&ProjectSnapshot.save_snapshot/1)
  end
end
