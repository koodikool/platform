defmodule Platform.Models.DayOfCode do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "days_of_code" do
    field :day, :integer
    field :status_id, :string
    field :user_id, :string

    timestamps()
  end

  @doc false
  def changeset(day_of_code, attrs) do
    day_of_code
    |> cast(attrs, [:user_id, :status_id, :day])
    |> validate_required([:user_id, :status_id, :day])
  end

  def parse_day(text) when is_bitstring(text) do
    text =
      text
      |> String.slice(0..-25)
      |> emoji_to_text()

    Regex.run(~r/((day ?)|D)(\d{1,3})/i, text)
    |> case do
      nil -> nil
      match -> Enum.at(match, -1) |> String.to_integer()
    end
  end

  def emoji_to_text(str) do
    number_table = %{
      "zero" => "0",
      "one" => "1",
      "two" => "2",
      "three" => "3",
      "four" => "4",
      "five" => "5",
      "six" => "6",
      "seven" => "7",
      "eight" => "8",
      "nine" => "9",
      "ten" => "10"
    }

    for c <- String.graphemes(str) do
      c
      |> Exmoji.char_to_unified()
      |> Exmoji.from_unified()
      |> case do
        nil -> c
        %{short_name: short_name} -> Map.get(number_table, short_name, c)
      end
    end
    |> Enum.join()
  end
end
