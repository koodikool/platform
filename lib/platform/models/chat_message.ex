defmodule Platform.Models.ChatMessage do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "chat_messages" do
    field :message, :string
    field :parent_id, :binary_id
    field :parent_type, :string
    field :from, :binary_id

    timestamps()
  end

  @doc false
  def changeset(chat_message, attrs) do
    chat_message
    |> cast(attrs, [:parent_type, :parent_id, :message, :from])
    |> validate_required([:parent_type, :parent_id, :message, :from])
  end
end
