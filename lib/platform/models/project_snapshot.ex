defmodule Platform.Models.ProjectSnapshot do
  alias Platform.ProjectState
  alias Platform.ProjectLessons
  alias Platform.ProjectStateAgent
  alias Platform.Models
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @derive {Jason.Encoder, only: [:project]}
  schema "project_snapshots" do
    field :project_id, :binary_id
    field :version, :integer, default: 3
    field :has_updates, :boolean, default: false
    field :project, :map

    timestamps()
  end

  @doc false
  def changeset(project_snapshot, attrs) do
    project_snapshot
    |> cast(attrs, [:project_id, :project, :has_updates])
    |> validate_required([:project_id, :project])
    |> foreign_key_constraint(:project_id)
  end

  def migrate_snapshot(%{version: 1} = snapshot) do
    %{
      "project_template_id" => project_template_id,
      "lesson_hash" => lesson_hash
    } = snapshot.project

    %{progressStr: progressStr} = ProjectLessons.get_lesson(project_template_id, lesson_hash)

    project =
      snapshot.project
      |> Map.put("progressStr", progressStr)
      |> Map.delete("progress")

    snapshot
    |> Map.put(:project, project)
    |> Map.put(:version, 2)
    |> migrate_snapshot()
  end

  def migrate_snapshot(%{version: 2} = snapshot) do
    project =
      snapshot.project
      |> Map.put("hints", %{
        "html" => 0,
        "css" => 0,
        "js" => 0
      })

    snapshot
    |> Map.put(:project, project)
    |> Map.put(:version, 3)
    |> migrate_snapshot()
  end

  def migrate_snapshot(snapshot) do
    snapshot
  end

  @spec save_snapshot(String.t() | ProjectState.t()) :: any
  def save_snapshot(project_id) when is_bitstring(project_id) do
    project_id
    |> ProjectStateAgent.get()
    |> save_snapshot()
  end

  def save_snapshot(%ProjectState{} = project) do
    code = Map.from_struct(project.code)
    hints = Map.from_struct(project.hints)

    project_map =
      project
      |> Map.from_struct()
      |> Map.put(:code, code)
      |> Map.put(:hints, hints)

    has_updates =
      project.has_updates ||
        project.asking_help ||
        project.chat_unread ||
        project.ready_next

    Models.create_project_snapshot(%{
      project_id: project.project_id,
      project: project_map,
      has_updates: has_updates
    })
  end
end
