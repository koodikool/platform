defmodule Platform.Models.ProjectTemplate do
  use Ecto.Schema
  import Ecto.Changeset
  alias Platform.Models
  alias Platform.Models.Project
  alias Platform.Models.ProjectTemplate

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "project_templates" do
    field :description, :string
    field :git_repo, :string
    field :public, :boolean, default: false
    field :slug, :string
    field :title, :string
    field :created_by, :binary_id

    timestamps()
  end

  @doc false
  def changeset(project_template, attrs) do
    project_template
    |> cast(attrs, [:git_repo, :public, :title, :description, :created_by])
    |> validate_required([:git_repo, :public, :title, :description, :created_by])
    |> force_change(:slug, add_slug(attrs))
    |> foreign_key_constraint(:created_by)
  end

  defp add_slug(%{git_repo: git_repo}) do
    add_slug(%{"git_repo" => git_repo})
  end

  defp add_slug(%{"git_repo" => git_repo}) do
    git_repo
    |> String.replace(~r/^https?:\/\//, "")
    |> String.replace(~r/\.com\//, "/")
    |> String.replace_suffix(".git", "")
    |> String.trim_leading("/")
  end

  defp add_slug(%{}) do
    nil
  end

  @doc ~S"""
  Convert id's into ProjectTemplates. ProjectTemplates and Projects are ignored.

  ## Examples

  Simply fetch all templates in list.

      iex> template = Models.list_project_templates() |> List.first()
      ...> get_all_in_order!([template.id])
      [template]

  ProjectTemplates and Projects are ignored.

      iex> template = Models.list_project_templates() |> List.first()
      ...> get_all_in_order!([%ProjectTemplate{}, %Project{}, template.id])
      [%ProjectTemplate{}, %Project{}, %ProjectTemplate{} = template]

  """
  @spec get_all_in_order!(list) :: [%ProjectTemplate{}]
  def get_all_in_order!(template_ids) do
    for project_template_id <- template_ids do
      get(project_template_id)
    end
  end

  @spec get(String.t() | %ProjectTemplate{} | %Project{}) :: %ProjectTemplate{} | %Project{}
  def get(id) when is_bitstring(id) and byte_size(id) == 36 do
    Models.get_project_template!(id)
  end

  def get(%ProjectTemplate{} = any) do
    any
  end

  def get(%Project{} = any) do
    any
  end
end
