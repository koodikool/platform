defmodule Platform.Models.Project do
  use Ecto.Schema
  import Ecto.Changeset
  alias Platform.Models.Project
  alias Platform.Models.ProjectTemplate

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "projects" do
    field :free_flow, :boolean, default: true
    field :created_by, :binary_id
    field :project_template_id, :binary_id
    field :room_id, :binary_id

    timestamps()
  end

  @doc false
  def changeset(project, attrs) do
    project
    |> cast(attrs, [:free_flow, :created_by, :project_template_id, :room_id])
    |> validate_required([:free_flow, :created_by, :project_template_id])
    |> foreign_key_constraint(:project_template_id)
    |> foreign_key_constraint(:created_by)
    |> foreign_key_constraint(:room_id)
  end

  @doc ~S"""
  Replaces each ProjectTemplate with Project in list. If there is no matching Project, leave ProjectTemplate as is.

  ## Examples

  Matching template_id will replace template with project. Extra projects will be discarded.

      iex> template_a = %ProjectTemplate{id: "a"}
      ...> template_b = %ProjectTemplate{id: "b"}
      ...> template_c = %ProjectTemplate{id: "c"}
      ...> project_b = %Project{project_template_id: "b"}
      ...> project_d = %Project{project_template_id: "d"}
      ...> replace_templates([template_a, template_b, template_c], [project_b, project_d])
      [template_a, project_b, template_c]

  """
  @spec replace_templates([%ProjectTemplate{}], [%Project{}]) :: [%ProjectTemplate{} | %Project{}]
  def replace_templates(project_templates, projects) do
    for template <- project_templates do
      Enum.find(projects, template, fn p -> p.project_template_id == template.id end)
    end
  end
end
