defmodule Platform.Models.Room do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "rooms" do
    field :name, :string
    field :created_by, :binary_id
    field :force_project_template_id, :binary_id
    field :free_flow, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(room, attrs) do
    room
    |> cast(attrs, [:name, :created_by, :force_project_template_id, :free_flow])
    |> validate_required([:name, :created_by])
    |> foreign_key_constraint(:created_by)
    |> foreign_key_constraint(:force_project_template_id)
  end
end
