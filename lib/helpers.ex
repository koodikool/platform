defmodule Platform.Helpers do
  def pop_at(list, index, default \\ nil)

  def pop_at(_list, nil, default) do
    default
  end

  def pop_at(list, index, default) do
    List.pop_at(list, index, default)
  end
end
