defmodule Platform.Repo.Migrations.CreateChatMessages do
  use Ecto.Migration

  def change do
    create table(:chat_messages, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :parent_type, :string
      add :parent_id, :binary_id
      add :message, :string
      add :from, references(:users, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:chat_messages, [:from])
    create index(:chat_messages, [:parent_type, :parent_id])
  end
end
