defmodule Platform.Repo.Migrations.AlterRoomsFreeFlow do
  use Ecto.Migration

  def change do
    alter table(:rooms) do
      add :free_flow, :boolean
    end
  end
end
