defmodule Platform.Repo.Migrations.AlterChatMessageLength do
  use Ecto.Migration

  def change do
    alter table(:chat_messages) do
      modify :message, :text
    end
  end
end
