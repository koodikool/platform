defmodule Platform.Repo.Migrations.AlterUsersRoom do
  use Ecto.Migration

  def change do
    alter table(:users, primary_key: false) do
      add :current_room_id, references(:rooms, on_delete: :nothing, type: :binary_id)
    end

    create index(:users, [:current_room_id])
  end
end
