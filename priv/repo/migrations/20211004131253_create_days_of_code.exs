defmodule Platform.Repo.Migrations.CreateDaysOfCode do
  use Ecto.Migration

  def change do
    create table(:days_of_code, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :user_id, :string
      add :status_id, :string
      add :day, :integer

      timestamps()
    end
  end
end
