defmodule Platform.Repo.Migrations.CreateProjects do
  use Ecto.Migration

  def change do
    create table(:projects, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :free_flow, :boolean, default: false, null: false
      add :created_by, references(:users, on_delete: :nothing, type: :binary_id)

      add :project_template_id,
          references(:project_templates, on_delete: :nothing, type: :binary_id)

      add :room_id, references(:rooms, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:projects, [:created_by])
    create index(:projects, [:project_template_id])
    create index(:projects, [:room_id])
  end
end
