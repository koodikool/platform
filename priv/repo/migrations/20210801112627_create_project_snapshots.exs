defmodule Platform.Repo.Migrations.CreateProjectSnapshots do
  use Ecto.Migration

  def change do
    create table(:project_snapshots, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :version, :integer
      add :project_id, references(:projects, on_delete: :nothing, type: :binary_id)
      add :project, :map

      timestamps()
    end

    create index(:project_snapshots, [:project_id])
  end
end
