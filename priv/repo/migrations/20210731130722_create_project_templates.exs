defmodule Platform.Repo.Migrations.CreateProjectTemplates do
  use Ecto.Migration

  def change do
    create table(:project_templates, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :git_repo, :string
      add :slug, :string
      add :public, :boolean, default: false, null: false
      add :title, :string
      add :description, :string
      add :created_by, references(:users, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:project_templates, [:created_by])
  end
end
