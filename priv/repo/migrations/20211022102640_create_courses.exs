defmodule Platform.Repo.Migrations.CreateCourses do
  use Ecto.Migration

  def change do
    create table(:courses, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :title, :string
      add :project_templates, {:array, :binary_id}

      timestamps()
    end
  end
end
