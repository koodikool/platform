defmodule Platform.Repo.Migrations.CreateRooms do
  use Ecto.Migration

  def change do
    create table(:rooms, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :created_by, references(:users, on_delete: :nothing, type: :binary_id)

      add :force_project_template_id,
          references(:project_templates, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:rooms, [:created_by])
    create index(:rooms, [:force_project_template_id])
  end
end
