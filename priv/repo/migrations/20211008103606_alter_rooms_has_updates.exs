defmodule Platform.Repo.Migrations.AlterRoomsHasUpdates do
  use Ecto.Migration

  def change do
    alter table(:project_snapshots) do
      add :has_updates, :boolean
    end
  end
end
